#ifndef SERVER_H
#define SERVER_H

#include <QTcpServer>
#include <QTcpSocket>
#include "storage.h"
#include "serialization.h"

#include <QObject>

class Server : public QObject
{
    QTcpServer * tcpServer;
    const int PORT = 3000;
    Storage * stor;

    int sessionId = 0;

    Q_OBJECT
public:
    explicit Server(QObject *parent = nullptr);

    void start();

    string getStringResponse(ReqRes req);

signals:

public slots:
};

#endif // SERVER_H
