#include "server.h"
#include <iostream>
#include <QCoreApplication>
#include <QDir>
#include <fstream>
#include <ctime>
#include <cstdlib>

using namespace std;

Server::Server(QObject *parent) : QObject(parent)
{
    tcpServer = new QTcpServer(this);

    if (!tcpServer->listen(QHostAddress::Any, PORT)) {
        cerr << "error listen " << endl;
    } else {
        cout << "started at " << PORT << endl;
        stor = new Storage();
        srand(unsigned(std::time(0)));
        sessionId = rand() % 10000;
        start();
    }
}

void Server::start() {
    string buffer = "";
    while (true)
    {
        if (tcpServer->waitForNewConnection(2000)) {
            QTcpSocket * client = tcpServer->nextPendingConnection();
            cout << "new connection accepted " << endl;
            bool hasRead = client->waitForReadyRead(2000);

            if (hasRead) {
                QByteArray bytes = client->readAll();
                buffer.append(bytes.toStdString());
                size_t pos = buffer.find('@');

                if (pos != string::npos) {
                    cout << "Received: " << endl << buffer << endl;
                    string reqStr = buffer.substr(0, pos - 1);
                    buffer.erase(0, pos + 1);
                    ReqRes req = deserializeRequest(reqStr);

                        string responseStr = getStringResponse(req);
                        cout << "Sending: " << endl << responseStr << endl;
                        QByteArray byteArray(responseStr.c_str(), responseStr.length());
                        client->write(byteArray);
                        client->flush();
                        client->waitForBytesWritten(2000);
                        client->close();
                        cout << "client closed" << endl;
                }
            }
        }
    }
    tcpServer->close();
}

string Server::getStringResponse(ReqRes req) {
    string responseStr = "empty response";
    QVariant var;
    if (req.getSessionId() == sessionId || req.getFunc() == FN_GET_FILES) {
        FunctionName func = req.getFunc();
        QVariant reqData = req.getData();
        switch (func) {
        case FN_GET_FILES: {
            var.setValue(stor->getFileNames());
            responseStr = serializeRespond(FN_GET_FILES, var, sessionId);
            break;
        }
        case FN_NEW_LIST: {
            stor->newList();
            responseStr = serializeRespond(FN_NEW_LIST, var, sessionId);
            break;
        }
        case FN_LOAD_LIST: {
            QString fileName = reqData.value<QString>();
            int status = stor->load(fileName);
            var.setValue(status);
            responseStr = serializeRespond(FN_LOAD_LIST, var, sessionId);
            break;
        }
        case FN_GET_DINOSAURS: {
            var.setValue(stor->get());
            responseStr = serializeRespond(FN_GET_DINOSAURS, var, sessionId);
            break;
        }
        case FN_SAVE_LIST: {
            QString fileName = reqData.value<QString>();
            int status = stor->save(fileName);
            var.setValue(status);
            responseStr = serializeRespond(FN_SAVE_LIST, var, sessionId);
            break;
        }
        case FN_ADD_DINOSAUR: {
            Dinosaur dino = reqData.value<Dinosaur>();
            int id = stor->add(dino);
            var.setValue(id);
            responseStr = serializeRespond(FN_ADD_DINOSAUR, var, sessionId);
            break;
        }
        case FN_UPDATE_DINOSAUR: {
            if (!reqData.canConvert<Dinosaur>()) {
                var.setValue(1);
            }
            else {
                Dinosaur dino = reqData.value<Dinosaur>();
                if (dino.getId() > 0 && dino.getId() <= stor->count()) {
                    stor->edit(dino.getId() - 1, dino);
                    var.setValue(0);
                }
            }
            responseStr = serializeRespond(FN_UPDATE_DINOSAUR, var, sessionId);
            break;
        }
        case FN_DELETE_DINOSAUR: {
            int id = reqData.value<int>();
            int status = 0;
            if (id <= 0 || id > stor->count())
                status = 1;
            else
                stor->remove(id - 1);
            var.setValue(status);
            responseStr = serializeRespond(FN_DELETE_DINOSAUR, var, sessionId);
            break;
        }
        default:
            break;
        }
    }
    else {
        responseStr = serializeRespond(FN_NONE, var, sessionId);
    }
    responseStr.push_back('@');
    return responseStr;
}
