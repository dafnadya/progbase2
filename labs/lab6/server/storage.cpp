#include "storage.h"

#include <fstream>
#include <QDir>
#include <QStringList>
#include <QtXml>
#include <iostream>
#include "serialization.h"

using namespace std;

Storage::Storage()
{
    list = QList<Dinosaur>();
    dirPath = "/home/nadiia/progbase2/labs/server/data";
}

string DinosaursToJsonString(QList<Dinosaur> & Dinosaurs) {
    QJsonDocument doc;
    QJsonArray dinosaursArr = DinosaursToJsonArray(Dinosaurs);
    doc.setArray(dinosaursArr);
    return doc.toJson().toStdString();
}

bool gotJsonError(string & str) {
    QJsonParseError err;
    QJsonDocument::fromJson(
           QByteArray::fromStdString(str),
           &err);
    if (err.error != QJsonParseError::NoError) {
        return 1;
    }
    return 0;
}

QList<Dinosaur> jsonStringToDinosaurs(string & str) {
    if (gotJsonError(str))
        return QList<Dinosaur>();
    QJsonDocument doc = QJsonDocument::fromJson(
           QByteArray::fromStdString(str));
    QJsonArray DinosaursArr = doc.array();
    QList<Dinosaur> list = jsonArrayToDinosaurs(DinosaursArr);

    return list;
}

int Storage::count() {
    return list.count();
}

int Storage::add(Dinosaur &dino) {
    Dinosaur d = dino;
    d.setId(list.count() + 1);
    list.push_back(d);
    return list.count();
}

void Storage::remove(int index) {
    list.removeAt(index);
    setIds();
}

void Storage::edit(int index, Dinosaur &dino) {
    list.replace(index, dino);
}

void Storage::newList() {
    list = QList<Dinosaur>();
}

void Storage::setIds() {
    for (int i = 0; i < list.count(); i++) {
        Dinosaur dino = list.at(i);
        dino.setId(i + 1);
        list.replace(i, dino);
    }
}

int Storage::load(QString fileName) {
    QString fullPath = dirPath + "/" + fileName;
    ifstream file(fullPath.toStdString());
    if(!file.is_open()) {
        return 1;
    }
    string str((istreambuf_iterator<char>(file)), istreambuf_iterator<char>());
    if (gotJsonError(str)) {
        return 1;
    }
    list = jsonStringToDinosaurs(str);
    setIds();
    file.close();
    return 0;
}

QList<Dinosaur> Storage::get() {
    return list;
}

int Storage::save(QString fileName) {
    QString fullPath = dirPath + "/" + fileName;
    ofstream file(fullPath.toStdString());
    if(!file.is_open()) {
        return 1;
    }
    string str = DinosaursToJsonString(this->list);
    file << str;
    file.close();
    return 0;
}

QList<QString> Storage::getFileNames() {
    QList<QString> list;
    QDir dir(dirPath);
    QStringList strList = dir.entryList(QStringList() << "*.json");
    foreach(QString fileName, strList) {
        list.push_back(fileName);
    }
    return list;
}
