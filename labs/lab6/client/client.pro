#-------------------------------------------------
#
# Project created by QtCreator 2018-04-23T16:17:41
#
#-------------------------------------------------

QT       += core gui xml network

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = lab5
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which as been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0


SOURCES += \
        main.cpp \
        mainwindow.cpp \
    adddialog.cpp \
    editdialog.cpp \
    serverconnect.cpp

HEADERS += \
        mainwindow.h \
    dinosaur.h \
    serialization.h \
    adddialog.h \
    editdialog.h \
    serverconnect.h

FORMS += \
        mainwindow.ui \
    adddialog.ui \
    editdialog.ui \
    serverconnect.ui

DISTFILES +=

win32:CONFIG(release, debug|release): LIBS += -L$$PWD/../build-dinosaur-unknown-Release/release/ -ldinosaur
else:win32:CONFIG(debug, debug|release): LIBS += -L$$PWD/../build-dinosaur-unknown-Release/debug/ -ldinosaur
else:unix: LIBS += -L$$PWD/../build-dinosaur-unknown-Release/ -ldinosaur

INCLUDEPATH += $$PWD/../common
DEPENDPATH += $$PWD/../common

win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$PWD/../build-dinosaur-unknown-Release/release/libdinosaur.a
else:win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$PWD/../build-dinosaur-unknown-Release/debug/libdinosaur.a
else:win32:!win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$PWD/../build-dinosaur-unknown-Release/release/dinosaur.lib
else:win32:!win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$PWD/../build-dinosaur-unknown-Release/debug/dinosaur.lib
else:unix: PRE_TARGETDEPS += $$PWD/../build-dinosaur-unknown-Release/libdinosaur.a
