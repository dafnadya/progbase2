#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QMessageBox>
#include <QCloseEvent>
#include <QFileDialog>
#include "adddialog.h"
#include "editdialog.h"
#include <QTcpSocket>
#include <QHostAddress>
#include "serialization.h"
#include "dinosaur.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

    int PORT;
    QTcpSocket * socket;
    QHostAddress serverAddress;

    int sessionId;

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
    void closeEvent(QCloseEvent *event);

    void setAddress(QString adress, int port);

private slots:

    void addNewDino(Dinosaur dino);

    void editDino(Dinosaur dino);

    void clearEdits();

    void enabledButtons();
    
    void on_listMain_itemSelectionChanged();
    
    void on_addButton_clicked();

    void on_removeButton_clicked();

    void on_editButton_clicked();

    void on_loadButton_clicked();

    void on_createNewButton_clicked();

    void on_fileList_itemSelectionChanged();

    void on_saveButton_clicked();

private:
    Ui::MainWindow *ui;

    void serverReboot();

    void errorConectingServer();

    void getFileList();

    void getDinoList();

    ReqRes getResp(FunctionName fn, QVariant var);

};

#endif // MAINWINDOW_H
