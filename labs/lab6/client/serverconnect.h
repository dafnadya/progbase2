#ifndef SERVERCONNECT_H
#define SERVERCONNECT_H

#include <QMainWindow>
#include <QHostAddress>
#include <QTcpSocket>
#include "mainwindow.h"


namespace Ui {
class ServerConnect;
}

class ServerConnect : public QMainWindow
{
    Q_OBJECT
    QTcpSocket * client;

public:
    explicit ServerConnect(QWidget *parent = 0);
    ~ServerConnect();

private slots:
    void on_pushButton_clicked();

public slots:

private:
    Ui::ServerConnect *ui;
};

#endif // SERVERCONNECT_H
