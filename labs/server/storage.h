#ifndef STORAGE_H
#define STORAGE_H

#include "dinosaur.h"
#include <QList>

class Storage
{
    QList<Dinosaur> list;

    QString dirPath;
public:
    Storage();

    int count();

    int add(Dinosaur &dino);
    void remove(int index);
    void edit(int index, Dinosaur &dino);

    void newList();
    void setIds();
    int load(QString fileName);
    QList<Dinosaur> get();
    int save(QString fileName);

    QList<QString> getFileNames();
};

#endif // STORAGE_H
