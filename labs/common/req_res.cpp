#include "req_res.h"

ReqRes::ReqRes(FunctionName func, QVariant data, int sessionId) {
    this->func = func;
    this->data = data;
    this->sessionId = sessionId;
}

FunctionName ReqRes::getFunc() {
    return this->func;
}

QVariant ReqRes::getData() {
    return this->data;
}

int ReqRes::getSessionId() {
    return this->sessionId;
}

void ReqRes::setFunc(FunctionName func) {
    this->func = func;
}

void ReqRes::setData(QVariant data) {
    this->data = data;
}

void ReqRes::setSessionId(int sessionId) {
    this->sessionId = sessionId;
}

QString FunctionNameToString(FunctionName func) {
    switch (func) {
    case FN_GET_FILES:
        return "FN_GET_FILES";
    case FN_NEW_LIST:
        return "FN_NEW_LIST";
    case FN_LOAD_LIST:
        return "FN_LOAD_LIST";
    case FN_SAVE_LIST:
        return "FN_SAVE_LIST";
    case FN_GET_DINOSAURS:
        return "FN_GET_DINOSAURS";
    case FN_ADD_DINOSAUR:
        return "FN_ADD_DINOSAUR";
    case FN_UPDATE_DINOSAUR:
        return "FN_UPDATE_DINOSAUR";
    case FN_DELETE_DINOSAUR:
        return "FN_DELETE_DINOSAUR";
    default:
        return "FN_NONE";
    }
}

FunctionName StringToFunctionName(QString func) {
    if (QString::compare(func, "FN_GET_FILES") == 0)
        return FN_GET_FILES;
    if (QString::compare(func,"FN_NEW_LIST") == 0)
        return FN_NEW_LIST;
    if (QString::compare(func,"FN_LOAD_LIST") == 0)
        return FN_LOAD_LIST;
    if (QString::compare(func,"FN_SAVE_LIST") == 0)
        return FN_SAVE_LIST;
    if (QString::compare(func,"FN_GET_DINOSAURS") == 0)
        return FN_GET_DINOSAURS;
    if (QString::compare(func,"FN_ADD_DINOSAUR") == 0)
        return FN_ADD_DINOSAUR;
    if (QString::compare(func,"FN_UPDATE_DINOSAUR") == 0)
        return FN_UPDATE_DINOSAUR;
    if (QString::compare(func,"FN_DELETE_DINOSAUR") == 0)
        return FN_DELETE_DINOSAUR;
    return FN_NONE;
}
