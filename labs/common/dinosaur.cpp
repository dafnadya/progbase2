#include "dinosaur.h"

Dinosaur::Dinosaur(QString name, int age, double length, double weight)
{
    this->name = name;
    this->age = age;
    this->length = length;
    this->weight = weight;
}
int Dinosaur::getId() {
    return this->id;
}

QString Dinosaur::getName() {
    return this->name;
}

int Dinosaur::getAge() {
    return this->age;
}

double Dinosaur::getLength() {
    return this->length;
}

double Dinosaur::getWeight() {
    return this->weight;
}

void Dinosaur::setId(int id) {
    this->id = id;
}

void Dinosaur::setName(QString name) {
    this->name = name;
}

void Dinosaur::setAge(int age) {
    this->age = age;
}

void Dinosaur::setLength(double length) {
    this->length = length;
}

void Dinosaur::setWeight(double weight) {
    this->weight = weight;
}

