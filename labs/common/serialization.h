#ifndef SERIALIZATION_H
#define SERIALIZATION_H

#include "req_res.h"
#include <vector>
#include <QtXml>
#include "dinosaur.h"

using namespace std;

string serializeRequest(FunctionName func, QVariant var, int sessionId);
string serializeRespond(FunctionName func, QVariant & var, int sessionId);
ReqRes deserializeRequest(string & str);
ReqRes deserializeRespond(string & str);

QJsonArray DinosaursToJsonArray(QList<Dinosaur> & Dinosaurs);
QList<Dinosaur> jsonArrayToDinosaurs(QJsonArray & dinoArr);

#endif // SERIALIZATION_H
