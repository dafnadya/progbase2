#ifndef FUNCTIONNAME_H
#define FUNCTIONNAME_H

#include <QString>
#include <QVariant>
#include <iostream>
#include <fstream>

typedef enum {
   FN_NONE,
   // supported remote functions
    FN_ERROR,
   FN_GET_FILES,
   FN_NEW_LIST,
   FN_LOAD_LIST,
   FN_SAVE_LIST,
   FN_GET_DINOSAURS,
   FN_ADD_DINOSAUR,
   FN_UPDATE_DINOSAUR,
   FN_DELETE_DINOSAUR
} FunctionName;

class ReqRes
{
    FunctionName func;
    QVariant data;
    int sessionId;

public:
    ReqRes() {}
    ReqRes(FunctionName func, QVariant data, int sessionId);

    FunctionName getFunc();
    QVariant getData();
    int getSessionId();

    void setFunc(FunctionName func);
    void setData(QVariant data);
    void setSessionId(int sessionId);
};

QString FunctionNameToString(FunctionName func);
FunctionName StringToFunctionName(QString func);

#endif // FUNCTIONNAME_H
