#include "serialization.h"

#include <fstream>
#include <iostream>

using namespace std;

QJsonArray DinosaursToJsonArray(QList<Dinosaur> & Dinosaurs) {
    QList<Dinosaur> list = Dinosaurs;
    QJsonArray dinosaursArr;

    for(Dinosaur  & l: list){
        QJsonObject dinosaurObj;
        dinosaurObj.insert("id", l.getId());
        dinosaurObj.insert("name", l.getName());
        dinosaurObj.insert("age", l.getAge());
        dinosaurObj.insert("length", l.getLength());
        dinosaurObj.insert("weight", l.getWeight());
        dinosaursArr.push_back(dinosaurObj);
    }
    return dinosaursArr;
}

QList<Dinosaur> jsonArrayToDinosaurs(QJsonArray & dinoArr) {
    QList<Dinosaur> list;
    QJsonArray DinosaursArr = dinoArr;
    for (int i = 0; i < DinosaursArr.size(); i++) {
        Dinosaur lect;
        QJsonValue value = DinosaursArr.at(i);
        QJsonObject lectObj = value.toObject();
        lect.setId(lectObj.value("id").toInt());
        lect.setName(lectObj.value("name").toString());
        lect.setAge(lectObj.value("age").toInt());
        lect.setLength(lectObj.value("length").toDouble());
        lect.setWeight(lectObj.value("weight").toDouble());
        list.push_back(lect);
     }

    return list;
}

string serializeRequest(FunctionName func, QVariant var, int sessionId) {
    QVariant variant = var;
    QJsonDocument doc;
    QJsonObject reqObj;
    reqObj.insert("function", FunctionNameToString(func));
    reqObj.insert("sessionId", sessionId);
    switch (func) {
    case FN_ADD_DINOSAUR: case FN_UPDATE_DINOSAUR: {
        Dinosaur d = variant.value<Dinosaur>();
        QJsonObject dinosaurObj;
        dinosaurObj.insert("id", d.getId());
        dinosaurObj.insert("name", d.getName());
        dinosaurObj.insert("age", d.getAge());
        dinosaurObj.insert("length", d.getLength());
        dinosaurObj.insert("weight", d.getWeight());
        reqObj.insert("dinosaur", dinosaurObj);
        break;
    }
    case FN_DELETE_DINOSAUR: {
        int id = variant.value<int>();
        reqObj.insert("id", id);
        break;
    }
    case FN_LOAD_LIST: case FN_SAVE_LIST: {
        QString fname = variant.value<QString>();
        reqObj.insert("fileName", fname);
        break;
    }
    default:
        break;
    }
    doc.setObject(reqObj);
    return doc.toJson().toStdString();
}

string serializeRespond(FunctionName func, QVariant & var, int sessionId) {
    QVariant variant = var;
    QJsonDocument doc;
    QJsonObject reqObj;
    reqObj.insert("function", FunctionNameToString(func));
    reqObj.insert("sessionId", sessionId);
    switch (func) {
    case FN_ADD_DINOSAUR: {
        int id = variant.value<int>();
        reqObj.insert("id", id);
        break;
    }
    case FN_DELETE_DINOSAUR: case FN_UPDATE_DINOSAUR:
    case FN_LOAD_LIST: case FN_SAVE_LIST: {
        int status = variant.value<int>();
        reqObj.insert("status", status);
        break;
    }
    case FN_GET_DINOSAURS: {
        QList<Dinosaur> list = variant.value<QList<Dinosaur>>();
        QJsonArray arrayDinos = DinosaursToJsonArray(list);
        reqObj.insert("dinoArray", arrayDinos);
        break;
    }
    case FN_GET_FILES: {
        QList<QString> list = variant.value<QList<QString>>();
        QJsonArray arrayFiles;
        for(QString  & s: list){
            arrayFiles.push_back(s);
        }
        reqObj.insert("fileNames", arrayFiles);
        break;
    }
    default:
        break;
    }
    doc.setObject(reqObj);
    return doc.toJson().toStdString();
}

ReqRes deserializeRequest(string & str) {

    QJsonParseError err;
    ReqRes s = ReqRes();
    QJsonDocument doc = QJsonDocument::fromJson(
           QByteArray::fromStdString(str),
           &err);
    if (err.error != QJsonParseError::NoError) {
        return s;
    }
    QJsonObject reqObj = doc.object();
    int sessionId = reqObj.value("sessionId").toInt();
    s.setSessionId(sessionId);
    QString f = reqObj.value("function").toString();
    FunctionName func = StringToFunctionName(f);
    s.setFunc(func);
    QVariant var;
    switch (func) {
    case FN_ADD_DINOSAUR: case FN_UPDATE_DINOSAUR: {
        Dinosaur d;
        QJsonObject dinoObj = reqObj.value("dinosaur").toObject();
        d.setId(dinoObj.value("id").toInt());
        d.setName(dinoObj.value("name").toString());
        d.setAge(dinoObj.value("age").toInt());
        d.setLength(dinoObj.value("length").toDouble());
        d.setWeight(dinoObj.value("weight").toDouble());
        var.setValue(d);
        break;
    }
    case FN_DELETE_DINOSAUR: {
        int id = reqObj.value("id").toInt();
        var.setValue(id);
        break;
    }
    case FN_LOAD_LIST: case FN_SAVE_LIST: {
        QString fileName = reqObj.value("fileName").toString();
        var.setValue(fileName);
        break;
    }
    default:
        break;
    }
    s.setData(var);
    return s;
}

ReqRes deserializeRespond(string & str) {
    QJsonParseError err;
    ReqRes s = ReqRes();
    QJsonDocument doc = QJsonDocument::fromJson(
           QByteArray::fromStdString(str),
           &err);
    if (err.error != QJsonParseError::NoError) {
        return s;
    }
    QJsonObject reqObj = doc.object();
    int sessionId = reqObj.value("sessionId").toInt();
    s.setSessionId(sessionId);
    QString f = reqObj.value("function").toString();
    FunctionName func = StringToFunctionName(f);
    s.setFunc(func);
    QVariant var;
    switch (func) {
    case FN_ADD_DINOSAUR: {
        int id = reqObj.value("id").toInt();
        var.setValue(id);
        break;
    }
    case FN_DELETE_DINOSAUR: case FN_UPDATE_DINOSAUR:
    case FN_LOAD_LIST: case FN_SAVE_LIST: {
        int status = reqObj.value("status").toInt();
        var.setValue(status);
        break;
    }
    case FN_GET_DINOSAURS: {
        QJsonArray arrayDinos = reqObj.value("dinoArray").toArray();
        QList<Dinosaur> list = jsonArrayToDinosaurs(arrayDinos);
        var.setValue(list);
        break;
    }
    case FN_GET_FILES: {
        QJsonArray arrayFiles = reqObj.value("fileNames").toArray();
        QList<QString> list;
        for(auto s: arrayFiles){
            list.push_back(s.toString());
        }
        var.setValue(list);
        break;
    }
    default:
        break;
    }
    s.setData(var);
    return s;
}
