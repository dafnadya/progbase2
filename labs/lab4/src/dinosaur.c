#include "../include/dinosaur.h"

static void checkNULL(void *self)
{
    if (self == NULL)
    {
        fwrite("Out of memory", 1, 14, stderr);
        abort();
    }
}

Parameters *Parameters_new(float length, float weight)
{
    Parameters *self = malloc(sizeof(Parameters));
    checkNULL(self);
    self->length = length;
    self->weight = weight;
    return self;
}

void Parameters_free(Parameters *self)
{
    free(self);
}

Dinosaur *Dinosaur_new(char * name, int age, float length, float weight)
{
    Dinosaur *self = malloc(sizeof(Dinosaur));
    checkNULL(self);
    self->age = age;
    self->name = malloc(sizeof(char) * (strlen(name) + 1));
    self->name[0] = '\0'; 
    if (self->name == NULL)
    {
        fwrite("Out of memory", 1, 14, stderr);
        abort();
    }
    strcat(self->name, name);
    self->param = Parameters_new(length, weight);
    return self;
}

void Dinosaur_free(Dinosaur *self)
{
    Parameters_free(self->param);
    free(self->name);
    free(self);
}