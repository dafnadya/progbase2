#include <string.h>
#include <string_buffer.h>
#include <assert.h>
#include <stdarg.h>

static const int INITIAL_CAPACITY = 256;

struct __StringBuffer
{
    char *buffer;
    size_t capacity;
    size_t length;
};

char *strdup(const char *str)
{
    char *dup = malloc(strlen(str) + 1);
    if (dup == NULL)
    {
        fwrite("Out of memory", 1, 14, stderr);
        abort();
    }
    strcpy(dup, str);
    return dup;
}

static void StringBuffer_checkNULL(StringBuffer *self)
{
    if (self == NULL)
    {
        fwrite("Out of memory", 1, 14, stderr);
        abort();
    }
}

StringBuffer *StringBuffer_new(void)
{
    StringBuffer *self = malloc(sizeof(StringBuffer));
    StringBuffer_checkNULL(self);
    self->capacity = INITIAL_CAPACITY;
    self->buffer = malloc(sizeof(char) * self->capacity);
    if (self->buffer == NULL)
    {
        fwrite("Out of memory", 1, 14, stderr);
        abort();
    }
    self->buffer[0] = '\0';
    self->length = 1;
    return self;
}

void StringBuffer_free(StringBuffer *self)
{
    StringBuffer_checkNULL(self);
    free(self->buffer);
    free(self);
}

static void ensureCapacity(StringBuffer *self, int appendLength)
{
    if (self->length + appendLength > self->capacity)
    {
        size_t newCapacity = self->capacity * 2;
        char *newBuffer = realloc(self->buffer, sizeof(char) * newCapacity);
        if (newBuffer == NULL)
        {
            assert(0 && "NULL pointer");
        }
        else
        {
            self->buffer = newBuffer;
            self->capacity = newCapacity;
        }
    }
}

void StringBuffer_append(StringBuffer *self, const char *str)
{
    StringBuffer_checkNULL(self);
    size_t strLen = strlen(str);
    ensureCapacity(self, strLen);
    strcat(self->buffer + (self->length - 1), str);
    self->length += strLen;
}
void StringBuffer_appendChar(StringBuffer *self, char ch)
{
    StringBuffer_checkNULL(self);
    ensureCapacity(self, 1);
    self->buffer[self->length] = '\0';
    self->buffer[self->length - 1] = ch;
    self->length++;
}
void StringBuffer_appendFormat(StringBuffer *self, const char *fmt, ...)
{
    va_list vlist;
    va_start(vlist, fmt);
    size_t bufSize = vsnprintf(NULL, 0, fmt, vlist);
    char * buf = malloc(bufSize + 1);
    va_start(vlist, fmt);
    vsnprintf(buf, bufSize + 1, fmt, vlist);
    va_end(vlist);
    StringBuffer_append(self, buf);
    free(buf);
}
void StringBuffer_clear(StringBuffer *self)
{
    StringBuffer_checkNULL(self);
    self->buffer[0] = '\0';
    self->length = 1;
}
char *StringBuffer_toNewString(StringBuffer *self)
{
    StringBuffer_checkNULL(self);
    return strdup(self->buffer);
}