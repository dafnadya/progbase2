#include "../include/csv.h"
#include "../include/string_buffer.h"
#include "../include/stdbool.h"
#include "../include/menu.h"
#include <assert.h>

typedef struct __CsvTable
{
    List *rows;
} CsvTable;

typedef struct __CsvRow
{
    List *values;
} CsvRow;

CsvRow *CsvRow_new(void)
{
    CsvRow *new = malloc(sizeof(CsvRow));
    new->values = List_new();
    return new;
}
void CsvRow_free(CsvRow *self)
{
    free(self);
}
void CsvRow_add(CsvRow *self, const char *value)
{
    List_add(self->values, (void *)value);
}

void List_CopyTo(List *self, List *dest)
{
    int count = List_count(self);
    for (int i = 0; i < count; i++)
    {
        void *value = List_at(self, i);
        List_add(dest, value);
    }
}

void CsvRow_values(CsvRow *self, List *values)
{
    List_CopyTo(self->values, values);
}

CsvTable *CsvTable_new(void)
{
    CsvTable *self = malloc(sizeof(CsvTable));
    self->rows = List_new();
    return self;
}
void CsvTable_free(CsvTable *self)
{
    List_free(self->rows);
    free(self);
}
void CsvTable_add(CsvTable *self, CsvRow *row)
{
    List_add(self->rows, (void *)row);
}
void CsvTable_rows(CsvTable *self, List *rows)
{
    List_CopyTo(self->rows, rows);
}

CsvTable *CsvTable_newFromList(List *self)
{
    CsvTable *t = CsvTable_new();
    CsvRow *row = CsvRow_new();
    for (int i = 0; i < List_count(self); i++)
    {
        Dinosaur *dino = List_at(self, i);
        CsvRow_add(row, dino->name);
        
        char ageStr[50];
        sprintf(ageStr, "%d", dino->age);
        char * age = malloc(sizeof(char) * (strlen(ageStr) + 1));
        age[0] = '\0';
        strcat(age, ageStr);
        CsvRow_add(row, age);

        char lenStr[50];
        sprintf(lenStr, "%f", dino->param->length);
        char * len = malloc(sizeof(char) * (strlen(lenStr) + 1));
        len[0] = '\0';
        strcat(len, lenStr);
        CsvRow_add(row, len);
        
        char weightStr[50];
        sprintf(weightStr, "%f", dino->param->weight);
        char * weight = malloc(sizeof(char) * (strlen(weightStr) + 1));
        weight[0] = '\0';
        strcat(weight, weightStr);
        CsvRow_add(row, weight);

        CsvTable_add(t, row);
        row = CsvRow_new();
    }
    return t;
}

CsvTable *CsvTable_newFromString(const char *csvString)
{
    CsvTable *t = CsvTable_new();
    CsvRow *row = CsvRow_new();
    StringBuffer *value = StringBuffer_new();
    while (true)
    {
        char ch = *csvString;
        if (ch == '\0')
        {
            char *newValue = StringBuffer_toNewString(value);
            CsvRow_add(row, newValue);
            StringBuffer_clear(value);

            CsvTable_add(t, row);
            break;
        }
        else if (ch == '\n')
        {
            char *newValue = StringBuffer_toNewString(value);
            CsvRow_add(row, newValue);
            StringBuffer_clear(value);

            CsvTable_add(t, row);
            row = CsvRow_new();
        }
        else if (ch == ',')
        {
            char *newValue = StringBuffer_toNewString(value);
            CsvRow_add(row, newValue);
            StringBuffer_clear(value);
        }
        else
        {
            StringBuffer_appendChar(value, ch);
        }
        csvString++;
    }
    StringBuffer_free(value);
    return t;
}

List *CsvTable_toNewList(CsvTable *self)
{
    List *list = List_new();
    for (int i = 0; i < List_count(self->rows); i++)
    {
        CsvRow *row = List_at(self->rows, i);
        char *name = List_at(row->values, 0);
        char *ageStr = List_at(row->values, 1);
        int age = atoi(ageStr);
        if (age <= 0)
            assert(0 && "Wrong age");
        char *lengthStr = List_at(row->values, 2);
        float length = atof(lengthStr);
        if (length <= 0)
            assert(0 && "Wrong length");
        char *weightStr = List_at(row->values, 3);
        float weight = atof(weightStr);
        if (weight <= 0)
            assert(0 && "Wrong weight");
        Dinosaur *dino = Dinosaur_new(name, age, length, weight);
        List_add(list, dino);
    }
    return list;
}

char *CsvTable_toNewString(CsvTable *self)
{
    StringBuffer *sb = StringBuffer_new();
    bool isFirstRow = true;
    for (int i = 0; i < List_count(self->rows); i++)
    {
        CsvRow *row = List_at(self->rows, i);
        if (isFirstRow)
            isFirstRow = false;
        else
            StringBuffer_appendChar(sb, '\n');
        bool isFirstValue = true;
        for (int j = 0; j < List_count(row->values); j++)
        {
            char *value = List_at(row->values, j);
            if (isFirstValue)
                isFirstValue = false;
            else
                StringBuffer_appendChar(sb, ',');
            StringBuffer_append(sb, value);
        }
    }

    char *copy = StringBuffer_toNewString(sb);
    StringBuffer_free(sb);
    return copy;
}