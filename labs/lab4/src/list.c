#include "../include/list.h"
#include <assert.h>

struct __List
{
    void **items;
    size_t length;
    size_t capacity;
};

static const size_t INITIAL_CAPACITY = 100;

static void checkNULL(void *self)
{
    if (self == NULL)
    {
        fwrite("Out of memory", 1, 14, stderr);
        abort();
    }
}

List *List_new(void)
{
    List *self = malloc(sizeof(List));
    checkNULL(self);
    self->capacity = INITIAL_CAPACITY;
    self->items = malloc(sizeof(void *) * self->capacity);
    if (self->items == NULL)
    {
        fwrite("Out of memory", 1, 14, stderr);
        abort();
    }
    self->length = 0;
    return self;
}

void List_free(List *self)
{
    checkNULL(self);
    free(self->items);
    free(self);
}

static void ensureCapacity(List *self)
{
    if (self->length == self->capacity)
    {
        size_t newCapacity = self->capacity * 2;
        void *newItems = realloc(self->items, sizeof(void *) * newCapacity);
        if (newItems == NULL)
        {
            assert(0 && "NULL pointer");
        }
        else
        {
            self->items = newItems;
            self->capacity = newCapacity;
        }
    }
}

void List_add(List *self, void *value)
{
    checkNULL(self);
    ensureCapacity(self);
    self->items[self->length] = value;
    self->length++;
}

void List_insert(List *self, void *value, size_t index)
{
    checkNULL(self);
    if (index >= self->capacity || index > self->length)
    {
        assert(0 && "Invalid index");
        fprintf(stderr, "Invalid index");
    }
    for (int i = self->length; i > index; i--)
    {
        self->items[i] = self->items[i - 1];
    }
    self->items[index] = value;
    self->length++;
}

void *List_removeAt(List *self, size_t index)
{
    checkNULL(self);
    if (index >= self->length)
    {
        assert(0 && "Invalid index");
        fprintf(stderr, "Invalid index");
    }
    assert(index < self->length);
    void *val = self->items[index];
    for (int i = index + 1; i < self->length; i++)
    {
        self->items[i - 1] = self->items[i];
    }
    self->length--;
    return val;
}

size_t List_count(List *self)
{
    checkNULL(self);
    return self->length;
}

void *List_at(List *self, size_t index)
{
    if (index >= self->length)
    {
        assert(0 && "Invalid index");
        fprintf(stderr, "Invalid index");
    }
    checkNULL(self);
    return self->items[index];
}

void * List_set(List *self, size_t index, void * value)
{
    if (self == NULL)
    {
        assert(0 && "Argument is NULL");
    }
    if (index >= self->length)
    {
        assert(0 && "Invalid index");
    }
    void * oldValue = List_removeAt(self, index);
    List_insert(self, value, index);
    return oldValue;
}