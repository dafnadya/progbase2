#define _GNU_SOURCE

#include "../include/cui.h"
#include "../include/menu.h"
#include <progbase/console.h>

#define __LEN(A) sizeof(A) / sizeof(A[0])

Command getCommandUserInput()
{
    Command command = NOTSET;
    do
    {
        char keyCode = Console_getChar();
        switch (keyCode)
        {
        case 127:
            command = BACK;
            break;
        case 65:
            command = UP;
            break;
        case 66:
            command = DOWN;
            break;
        case 10:
            command = ENTER;
            break;
        }
    } while (command == NOTSET);
    return command;
}

void printMainMenu(int operation) {
    Console_clear();
    printSmth("Choose option:\n\n");
    if (operation == 1)
        Console_setCursorAttribute(FG_CYAN);
    puts("Read struct from file");
    Console_setCursorAttribute(FG_DEFAULT);
    if (operation == 2)
        Console_setCursorAttribute(FG_CYAN);
    puts("Create new struct");
    Console_setCursorAttribute(FG_DEFAULT);
}

void printListMenu(int operation)
{
    printSmth("Choose operation\n(Press [Backspace] to return to main menu):\n\n");
    if (operation == 1)
        Console_setCursorAttribute(FG_CYAN);
    puts("Create dinosaur");
    Console_setCursorAttribute(FG_DEFAULT);
    if (operation == 2)
        Console_setCursorAttribute(FG_CYAN);
    puts("Edit dinosaur");
    Console_setCursorAttribute(FG_DEFAULT);
    if (operation == 3)
        Console_setCursorAttribute(FG_CYAN);
    puts("Find dinosaurs with weight less than X");
    Console_setCursorAttribute(FG_DEFAULT);
    if (operation == 4)
        Console_setCursorAttribute(FG_CYAN);
    puts("Save list to file");
    Console_setCursorAttribute(FG_DEFAULT);
}

void printEditMenu(List * dinosList, int operation)
{
    Console_clear();
    printDinosaurs(dinosList);
    Console_setCursorPosition(0, 0);
    printSmth("Choose operation\n(Press [Backspace] to return to list menu):\n\n");
    if (operation == 1)
        Console_setCursorAttribute(FG_CYAN);
    puts("Delete dinosaur in some position");
    Console_setCursorAttribute(FG_DEFAULT);
    if (operation == 2)
        Console_setCursorAttribute(FG_CYAN);
    puts("Change dinosaur");
    Console_setCursorAttribute(FG_DEFAULT);
    if (operation == 3)
        Console_setCursorAttribute(FG_CYAN);
    puts("Change some dinosaur's parameters");
    Console_setCursorAttribute(FG_DEFAULT);
}

void printSaveMenu(int operation) {
    Console_setCursorPosition(11, 0);
    printSmth("Save changes?");
    if (operation == 1)
        Console_setCursorAttribute(FG_CYAN);
    Console_setCursorPosition(12, 0);
    printf("Yes");
    Console_setCursorAttribute(FG_DEFAULT);
    if (operation == 2)
        Console_setCursorAttribute(FG_CYAN);
    Console_setCursorPosition(13, 0);
    printf("No\n");
    Console_setCursorAttribute(FG_DEFAULT);
}

void printDinosaurs(List * dinos)
{
    int count = List_count(dinos);
    Console_setCursorPosition(1, 50);
    printf("***You've got %i dinosaurs***", count);
    for (int i = 0, x = 2; i < count; i++)
    {
        Dinosaur * dino = List_at(dinos, i);
        Console_setCursorPosition(x, 50);
        Console_setCursorAttribute(FG_RED);
        printf("----------%i----------", i);
        x++;
        Console_setCursorAttribute(FG_DEFAULT);
        Console_setCursorPosition(x, 50);
        printf("Name: %s", dino->name);
        x++;
        Console_setCursorPosition(x, 50);
        printf("Age: %i BC", dino->age);
        x++;
        Console_setCursorPosition(x, 50);
        printf("Length: %.2f meters", dino->param->length);
        x++;
        Console_setCursorPosition(x, 50);
        printf("Weight: %.2f tons", dino->param->weight);
        x++;
    }
}

void printAll(List * dinosList, int operation) {
    Console_clear();
    printDinosaurs(dinosList);
    Console_setCursorPosition(0, 0);
    printListMenu(operation);
}

void printWantedDinosaurs(Dinosaur * dino, bool isWanted, int i, int x)
{
    if (isWanted)
            Console_setCursorAttribute(FG_MAGENTA);
        Console_setCursorPosition(x, 50);
        printf("----------%i----------", i);
        x++;
        Console_setCursorPosition(x, 50);
        printf("Name: %s", dino->name);
        x++;
        Console_setCursorPosition(x, 50);
        printf("Age: %i BC", dino->age);
        x++;
        Console_setCursorPosition(x, 50);
        printf("Length: %.2f meters", dino->param->length);
        x++;
        Console_setCursorPosition(x, 50);
        printf("Weight: %.2f tons", dino->param->weight);
        x++;
        if (isWanted)
            Console_setCursorAttribute(FG_DEFAULT);
}

void printSmth(char *str)
{
    Console_setCursorAttribute(FG_YELLOW);
    printf("%s", str);
    Console_setCursorAttribute(FG_DEFAULT);
}