#include "../include/file.h"
#include <progbase.h>

long getFileSize(FILE *f)
{
    fseek(f, 0, SEEK_END);
    long fsize = ftell(f);
    fseek(f, 0, SEEK_SET);
    return fsize;
}

char * getFromFile(void) {
    char fileName[100];
    printSmth("\nEnter name of file:\n");
    fgets(fileName, 99, stdin);
    fileName[strlen(fileName) - 1] = '\0';
    FILE *fp = fopen(fileName, "rb");

    if (fp == NULL)
    {
        printSmth("\nCannot open file.");
        fflush(stdout);
        sleepMillis(900);
        return NULL;
    }
    int len = getFileSize(fp);
    char * buffer = malloc(sizeof(char) * len);
    fread(buffer, len, 1, fp);
    fclose(fp);
    buffer[len] = '\0';
    return buffer;
}

int saveToFile(CsvTable * dinos)
{
    char fileName[50];
    printSmth("\nEnter destination file name:\n");
    fgets(fileName, 49, stdin);
    fileName[strlen(fileName) - 1] = '\0';
    FILE *fp = fopen(fileName, "wb");
    if (!fp) return 1;
    char * buffer = CsvTable_toNewString(dinos);
    fwrite(buffer, 1, strlen(buffer), fp);
    fclose(fp);
    free(buffer);
    return 0;
}