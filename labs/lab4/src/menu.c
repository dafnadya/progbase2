#include "../include/menu.h"
#include <progbase/console.h>

#define __LEN(A) sizeof(A) / sizeof(A[0])

static void cleanBuffer()
{
    char c = 0;
    while (c != '\n')
    {
        c = getchar();
    }
}

void chooseMainMenu()
{
    int option = 1;
    while (1)
    {
        printMainMenu(option);

        Command command = getCommandUserInput();

        switch (command)
        {
            {
            case UP:
                option -= 1;
                if (option < 1)
                {
                    option = 2;
                }
                break;
            case DOWN:
                option += 1;
                if (option > 2)
                {
                    option = 1;
                }
                break;
            case ENTER:
            {
                mainMenu(option);
                break;
            }
            case NOTSET:
            case BACK:
                return;
            }
        }
    }
    return;
}

void mainMenu(int option)
{
    if (option == 1) // list from file
    {
        char *buffer = getFromFile();
        if (strlen(buffer) == 0) option = 2;
        else if (buffer != NULL)
        {
            CsvTable *table = CsvTable_newFromString(buffer);
            free(buffer);
            List *dinosList = CsvTable_toNewList(table);
            CsvTable_free(table);
            chooseListMenu(dinosList);
            int count = List_count(dinosList);
            for (int i = 0; i < count; i++)
            {
                Dinosaur_free(List_at(dinosList, i));
            }
            List_free(dinosList);
        }
    }

    if (option == 2) // new list
    {
        List *dinosList = List_new();
        chooseListMenu(dinosList);
        int count = List_count(dinosList);
        for (int i = 0; i < count; i++)
        {
            Dinosaur_free(List_at(dinosList, i));
        }
        List_free(dinosList);
    }
    return;
}

void chooseListMenu(List *dinosList)
{
    int operation = 1;
    while (1)
    {
         if (operation != 3)
        {
             Console_clear();
             printDinosaurs(dinosList);
         }
         else
         {
             for (int i = 1; i < 6; i++)
             {
                 Console_setCursorPosition(i, 0);
                 puts("                                        ");
             }
        
         }

        Console_setCursorPosition(0, 0);
        printListMenu(operation);

        Command command = getCommandUserInput();

        switch (command)
        {
        case UP:
            operation -= 1;
            if (operation < 1)
            {
                operation = 4;
            }
            break;
        case DOWN:
            operation += 1;
            if (operation > 4)
            {
                operation = 1;
            }
            break;
        case ENTER:
        {
            listMenu(dinosList, operation);
            break;
        }
        case NOTSET:
        case BACK:
            return;
        }
    }
}

void chooseEditMenu(List *dinosList)
{
    int operation = 1;
    while (1)
    {
        printEditMenu(dinosList, operation);

        Command command = getCommandUserInput();

        switch (command)
        {
        case UP:
            operation -= 1;
            if (operation < 1)
            {
                operation = 3;
            }
            break;
        case DOWN:
            operation += 1;
            if (operation > 3)
            {
                operation = 1;
            }
            break;
        case ENTER:
        {
            editMenu(dinosList, operation);
            break;
        }
        case NOTSET:
        case BACK:
            return;
        }
    }
}

void listMenu(List *dinosList, int operation)
{
    if (operation == 1) // add dino
    {
        int index = -1;
        printSmth("\nEnter position:\n");
        scanf("%i", &index);
        cleanBuffer();
        if (index < 0 || List_count(dinosList) < index)
        {
            printSmth("\nInvalid index.");
            fflush(stdout);
            sleepMillis(900);
            return;
        }
        char name[100];
        int age = 0;
        float length = 0;
        float weight = 0;
        printSmth("Enter name:\n");
        fgets(name, 99, stdin);
        name[strlen(name) - 1] = '\0';
        printSmth("Enter age:\n");
        while (age <= 0)
        {
            scanf("%i", &age);
            cleanBuffer();
        }
        printSmth("Enter length:\n");
        while (length <= 0)
        {
            scanf("%f", &length);
            cleanBuffer();
        }
        printSmth("Enter weight:\n");
        while (weight <= 0)
        {
            scanf("%f", &weight);
            cleanBuffer();
        }
        Dinosaur *newDinosaur = Dinosaur_new(name, age, length, weight);
        List_insert(dinosList, newDinosaur, index);

        printAll(dinosList, operation);
        if (!saveMenu(dinosList))
        {
            Dinosaur *toRemove = List_removeAt(dinosList, index);
            Dinosaur_free(toRemove);
        }
    }
    else if (operation == 2) // edit dino
    {
       chooseEditMenu(dinosList);
    }
    else if (operation == 3) // find dino
    {
        float x = 0;
        printSmth("\nEnter X:\n");
        while (x <= 0)
        {
            scanf("%f", &x);
            cleanBuffer();
            Console_setCursorPosition(11, 0);
            puts("                                                 ");
            Console_setCursorPosition(11, 0);
        }
        int count = findDinosaurs(dinosList, x);
        Console_setCursorPosition(0, 50);
        printf("Found %i dinosaurs with weight less than %.2f:", count, x);
        x = 0;
        Console_setCursorPosition(0, 0);
        printListMenu(operation);
    }
    else if (operation == 4) // save to file
    {
        CsvTable *table = CsvTable_newFromList(dinosList);
        int existFile = saveToFile(table);
        CsvTable_free(table);
        if (existFile == 1)
        {
            printSmth("\nCannot open file.");
            fflush(stdout);
            sleepMillis(900);
            return;
        }
    }
    return;
}

void editMenu(List *dinosList, int operation)
{
    if (operation == 1) // remove dino
    {
        int index = -1;
        printSmth("\nEnter position:\n");
        scanf("%i", &index);
        cleanBuffer();
        if (index < 0 || List_count(dinosList) <= index)
        {
            printSmth("\nInvalid index.");
            fflush(stdout);
            sleepMillis(900);
            return;
        }
        Dinosaur *toRemove = List_removeAt(dinosList, index);

        printAll(dinosList, operation);
        if (saveMenu(dinosList))
            Dinosaur_free(toRemove);
        else
            List_insert(dinosList, toRemove, index);
    }
    else if (operation == 2) // change dino
    {
        int index = -1;
        printSmth("\nEnter index:\n");
        scanf("%i", &index);
        cleanBuffer();
        if (index < 0 || List_count(dinosList) <= index)
        {
            printSmth("\nInvalid index.");
            fflush(stdout);
            sleepMillis(900);
            return;
        }
        char name[100];
        int age = 0;
        float length = 0;
        float weight = 0;
        printSmth("*Enter new values*\nName:\n");
        fgets(name, 99, stdin);
        name[strlen(name) - 1] = '\0';
        printSmth("Age:\n");
        while (age <= 0)
        {
            scanf("%i", &age);
            cleanBuffer();
        }
        printSmth("Length:\n");
        while (length <= 0)
        {
            scanf("%f", &length);
            cleanBuffer();
        }
        printSmth("Weight:\n");
        while (weight <= 0)
        {
            scanf("%f", &weight);
            cleanBuffer();
        }
        changeStruct(dinosList, index, name, age, length, weight, operation);
    }
    else if (operation == 3) // change param
    {
        int index = -1;
        printSmth("\nEnter index:\n");
        scanf("%i", &index);
        cleanBuffer();
        if (index < 0 || index >= List_count(dinosList))
        {
            printSmth("\nInvalid index.");
            fflush(stdout);
            sleepMillis(900);
            return;
        }
        char field[10];
        printSmth("Enter parameter (name/age/length/weight):\n");
        fgets(field, 9, stdin);
        field[strlen(field) - 1] = '\0';
        bool changed = changeField(dinosList, index, field, operation);
        if (!changed)
        {
            printSmth("\nWrong parameter.");
            fflush(stdout);
            sleepMillis(900);
            return;
        }
    }
    return;
}

bool saveMenu(List *dinosList)
{
    int option = 1;
    while (1)
    {
        printSaveMenu(option);

        Command command = getCommandUserInput();

        switch (command)
        {
            {
            case UP:
                option -= 1;
                if (option < 1)
                {
                    option = 2;
                }
                break;
            case DOWN:
                option += 1;
                if (option > 2)
                {
                    option = 1;
                }
                break;
            case ENTER:
            {
                if (option == 1) // save
                {
                    return true;
                }

                else if (option == 2) // don't save
                {
                    return false;
                }
            }
            case NOTSET:
            case BACK:
                return false;
            }
        }
    }
}

bool changeStruct(List *dinosList, size_t index, char *name, int age, float length, float weight, int op)
{
    if (dinosList == NULL)
        return false;
    Dinosaur *newDinosaur = Dinosaur_new(name, age, length, weight);
    Dinosaur *oldDinosaur = List_set(dinosList, index, newDinosaur);
    
    printAll(dinosList, op);
        if (saveMenu(dinosList))
            Dinosaur_free(oldDinosaur);
        else {
            List_set(dinosList, index, oldDinosaur);
            Dinosaur_free(newDinosaur);
            return false;
        }
    return true;
}

bool changeField(List *dinosList, size_t index, char *field, int op)
{
    if (strcmp(field, "name") == 0)
    {
        char newName[100];
        printSmth("Enter new name:\n");
        fgets(newName, 99, stdin);
        newName[strlen(newName) - 1] = '\0';
        Dinosaur *dino = List_at(dinosList, index);
        
        char oldName[50];
        strcpy(oldName, dino->name);

        dino->name[0] = '\0';
        strcat(dino->name, newName);
        
        printAll(dinosList, op);
        if (!saveMenu(dinosList)) {
            dino->name[0] = '\0';
            strcat(dino->name, oldName);
        }
        
        return true;
    }
    else if (strcmp(field, "age") == 0)
    {
        Dinosaur *dino = List_at(dinosList, index);
        printSmth("Enter new age:\n");
        int oldAge = dino->age;
        dino->age = 0;
        while (dino->age <= 0)
        {
            scanf("%i", &dino->age);
            cleanBuffer();
        }
        printAll(dinosList, op);
        if (!saveMenu(dinosList)) {
            dino->age = oldAge;
        }
        return true;
    }
    else if (strcmp(field, "weight") == 0)
    {
        Dinosaur *dino = List_at(dinosList, index);
        printSmth("Enter new weight:\n");
        float oldWeight = dino->param->weight;
        dino->param->weight = 0;
        while (dino->param->weight <= 0)
        {
            scanf("%f", &dino->param->weight);
            cleanBuffer();
        }
        printAll(dinosList, op);
        if (!saveMenu(dinosList)) {
            dino->param->weight = oldWeight;
        }
        return true;
    }
    else if (strcmp(field, "length") == 0)
    {
        Dinosaur *dino = List_at(dinosList, index);
        printSmth("Enter new length:\n");
        float oldLength = dino->param->length;
        dino->param->length = 0;
        while (dino->param->length <= 0)
        {
            scanf("%f", &dino->param->length);
            cleanBuffer();
        }
        printAll(dinosList, op);
        if (!saveMenu(dinosList)) {
            dino->param->length = oldLength;
        }
        return true;
    }
    else
        return false;
}

int findDinosaurs(List *dinosList, float wantedWeigth)
{
    int countAll = List_count(dinosList);
    int countWanted = 0;
    Console_clear();
    Console_setCursorPosition(1, 50);
    for (int i = 0, x = 2; i < countAll; i++, x += 5)
    {
        Dinosaur *dino = List_at(dinosList, i);
        if (dino->param->weight < wantedWeigth)
        {
            printWantedDinosaurs(dino, true, i, x);
            countWanted++;
        }
        else
            printWantedDinosaurs(dino, false, i, x);
    }
    return countWanted;
}