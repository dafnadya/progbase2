#pragma once

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "cui.h"
#include "csv.h"

long getFileSize(FILE *f);
char * getFromFile(void);
int saveToFile(CsvTable * dinos);