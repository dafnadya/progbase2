#pragma once

#include <stdlib.h>
#include <stdio.h>
#include <limits.h>
#include <ctype.h>
#include <string.h>
#include <stdbool.h>
#include <math.h>
#include <assert.h>
#include <progbase.h>
#include "csv.h"
#include "cui.h"
#include "file.h"
#include "dinosaur.h"

void chooseMainMenu();
void chooseListMenu(List *dinosList);
void chooseEditMenu(List *dinosList);

void mainMenu(int option);
void listMenu(List *dinosList, int operation);
void editMenu(List *dinosList, int operation);
bool saveMenu(List * dinosCopy);

bool changeStruct(List * dinosList, size_t index, char *name, int age, float length, float weight, int op);
bool changeField(List * dinosList, size_t index, char *field, int op);
int findDinosaurs(List * dinosList, float wantedWeigth);