#pragma once

#include "list.h"

typedef struct __CsvTable CsvTable;
typedef struct __CsvRow   CsvRow;

CsvRow * CsvRow_new(void);
void CsvRow_free(CsvRow * self);
void CsvRow_add(CsvRow * self, const char * value);
void CsvRow_values(CsvRow * self, List * values);

void List_CopyTo(List *self, List *dest);

CsvTable * CsvTable_new(void);
void CsvTable_free(CsvTable * self);
void CsvTable_add (CsvTable * self, CsvRow * row);
void CsvTable_rows(CsvTable * self, List * rows);

CsvTable * CsvTable_newFromString(const char * csvString);
char *     CsvTable_toNewString  (CsvTable * self);

List * CsvTable_toNewList(CsvTable *self);
CsvTable * CsvTable_newFromList(List *self);