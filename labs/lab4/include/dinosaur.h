#pragma once

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

typedef struct __Parameters Parameters;

struct __Parameters
{
    float length;
    float weight;
};

typedef struct __Dinosaur Dinosaur;

struct __Dinosaur
{
    char * name;
    int age;
    Parameters * param;
};

Parameters *Parameters_new(float length, float weight);
void Parameters_free(Parameters *self);
Dinosaur *Dinosaur_new(char * name, int age, float length, float weight);
void Dinosaur_free(Dinosaur *self);