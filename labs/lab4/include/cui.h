#pragma once

#include "list.h"
#include "dinosaur.h"
#include <stdbool.h>

typedef enum {
    NOTSET,
    UP,    // 65
    DOWN,  // 66
    ENTER, // 10
    BACK   // 127
} Command;

int chooseOperation(List * dinos);
void printDinosaurs(List * dinos);
void printWantedDinosaurs(Dinosaur * dino, bool isWanted, int i, int x);
void printMainMenu(int operation);
void printListMenu(int operation);
void printEditMenu(List * dinosList, int operation);
void printSaveMenu(int operation);
void printAll(List * dinos, int operation);
void printSmth(char *str);
Command getCommandUserInput();