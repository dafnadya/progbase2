#include "serverconnect.h"
#include "ui_serverconnect.h"
#include "mainwindow.h"
#include <QAbstractSocket>

ServerConnect::ServerConnect(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::ServerConnect)
{
    ui->setupUi(this);

    client = new QTcpSocket(this);
}

ServerConnect::~ServerConnect()
{
    delete client;
    delete ui;
}

void ServerConnect::on_pushButton_clicked()
{
    QString adress = ui->adress->text();
    int port = ui->port->text().toInt();
    QHostAddress serverAddress(adress);
    client->connectToHost(serverAddress, port);
    if (!client->waitForConnected(2000)) {
        QMessageBox::critical(this,
                              "Error",
                              tr("Error connecting server"),
                              QMessageBox::Ok, QMessageBox::Ok);
    }
    else {
        client->disconnectFromHost();
        this->hide();
        MainWindow * wind = new MainWindow();
        wind->setAddress(adress, port);
        wind->show();
    }
}
