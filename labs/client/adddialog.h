#ifndef ADDDIALOG_H
#define ADDDIALOG_H

#include <QDialog>
#include "dinosaur.h"

namespace Ui {
class AddDialog;
}

class AddDialog : public QDialog
{
    Q_OBJECT

public:
    explicit AddDialog(QWidget *parent = 0);
    ~AddDialog();

private slots:

    void on_nameEl_textEdited(const QString &arg1);

    void on_okButton_clicked();

    void on_cancelButton_clicked();

private:
    Ui::AddDialog *ui;
signals:
    void addDino(Dinosaur dino);
};

#endif // ADDDIALOG_H
