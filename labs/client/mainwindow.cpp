#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    socket = new QTcpSocket(this);
    sessionId = -1;
}

MainWindow::~MainWindow()
{
    ui->fileList->clear();
    ui->listMain->clear();
    delete socket;
    delete ui;
}

void MainWindow::setAddress(QString adress, int port) {
    this->serverAddress = QHostAddress(adress);
    this->PORT = port;
    getFileList();
}

void MainWindow::errorConectingServer() {
    QMessageBox::critical(this,
                          "Error",
                          tr("Error connecting server"),
                          QMessageBox::Ok, QMessageBox::Ok);
}

void MainWindow::getFileList() {
    socket->connectToHost(serverAddress, PORT);
    if (!socket->waitForConnected(2000)) {
        errorConectingServer();
    }
    else {
        std::string str = serializeRequest(FN_GET_FILES, QVariant(), sessionId);
        str.push_back('@');
        QByteArray data(str.c_str(), str.length());
        socket->write(data);
        socket->flush();
        socket->waitForBytesWritten(2000);
        string buffer = "";
        if (socket->waitForReadyRead(2000)) {
            QByteArray bytes = socket->readAll();
            buffer.append(bytes.toStdString());
            size_t pos = buffer.find('@');
            while (pos == string::npos) {
                if (socket->waitForReadyRead(2000)) {
                    QByteArray bytes = socket->readAll();
                    buffer.append(bytes.toStdString());
                }
                pos = buffer.find('@');
            }
            string respond = buffer.substr(0, pos - 1);
            ReqRes r = deserializeRespond(respond);
            sessionId = r.getSessionId();
                QVariant var = r.getData();
                QList<QString> files = var.value<QList<QString>>();
                for (QString & str : files) {
                    QListWidgetItem *item = new QListWidgetItem;
                    item->setText(str);
                    ui->fileList->addItem(item);
                }
        }
        socket->disconnectFromHost();
    }
}

void MainWindow::closeEvent(QCloseEvent *event) {
    QMessageBox::StandardButton resBtn = QMessageBox::question(
                this,
                "On close",
                tr("Are you sure?\n"),
                QMessageBox::No | QMessageBox::Yes,
                QMessageBox::No);
    if (resBtn != QMessageBox::Yes) {
        event->ignore();
    }
    else {
        event->accept();
    }
}

void MainWindow::serverReboot() {
    ui->listMain->clear();
    ui->fileList->clear();
    QMessageBox::information(
                this,
                "Server reboot",
                "Server was reloaded",
                QMessageBox::Ok,
                QMessageBox::Ok);
    ui->dinoMenu->setEnabled(false);
    ui->saveButton->setEnabled(false);
    getFileList();
}

void MainWindow::getDinoList() {
    socket->connectToHost(serverAddress, PORT);
    if (!socket->waitForConnected(2000)) {
        errorConectingServer();
    }
    else {
        std::string str = serializeRequest(FN_GET_DINOSAURS, QVariant(), sessionId);
        str.push_back('@');
        QByteArray data(str.c_str(), str.length());
        socket->write(data);
        socket->flush();
        socket->waitForBytesWritten(2000);
        string buffer = "";
        if (socket->waitForReadyRead(2000)) {
            QByteArray bytes = socket->readAll();
            buffer.append(bytes.toStdString());
            size_t pos = buffer.find('@');
            while (pos == string::npos) {
                if (socket->waitForReadyRead(2000)) {
                    QByteArray bytes = socket->readAll();
                    buffer.append(bytes.toStdString());
                }
                pos = buffer.find('@');
            }
            string respond = buffer.substr(0, pos - 1);
            ReqRes r = deserializeRespond(respond);
            if(r.getSessionId() == sessionId) {
                ui->listMain->clear();
                QVariant var = r.getData();
                QList<Dinosaur> dinos = var.value<QList<Dinosaur>>();
                for (Dinosaur & d : dinos) {
                    QListWidgetItem *item = new QListWidgetItem;
                    QVariant dinoVar;
                    dinoVar.setValue(d);
                    item->setData(Qt::UserRole, dinoVar);
                    item->setText(d.getName());
                    ui->listMain->addItem(item);
                }
            }
        }
        socket->disconnectFromHost();
    }
}

void MainWindow::clearEdits() {
    ui->nameEdit->setText("");
    ui->ageEdit->setText("");
    ui->lengthEdit->setText("");
    ui->weightEdit->setText("");
}

void MainWindow::on_listMain_itemSelectionChanged()
{
    QList<QListWidgetItem *> selectedItem = ui->listMain->selectedItems();
    if (selectedItem.count() != 0) {
        ui->removeButton->setEnabled(true);
        if (selectedItem.count() == 1) {
            ui->editButton->setEnabled(true);
            QListWidgetItem * selected = selectedItem.at(0);
            QVariant var = selected->data(Qt::UserRole);
            Dinosaur d = var.value<Dinosaur>();
            ui->nameEdit->setText(d.getName());
            ui->ageEdit->setText(QString::number(d.getAge()) + " years");
            ui->lengthEdit->setText(QString::number(d.getLength()) + " sm");
            ui->weightEdit->setText(QString::number(d.getWeight()) + " kg");
        }
        else {
            ui->editButton->setEnabled(false);
            clearEdits();
        }
    }
    else {
        ui->removeButton->setEnabled(false);
        ui->editButton->setEnabled(false);
        clearEdits();
    }
}

ReqRes MainWindow::getResp(FunctionName fn, QVariant var) {
    ReqRes r;
    socket->connectToHost(serverAddress, PORT);
    if (!socket->waitForConnected(2000)) {
        errorConectingServer();
        r.setFunc(FN_ERROR);
        return r;
    }
    string str = serializeRequest(fn, var, sessionId);
    str.push_back('@');
    QByteArray data(str.c_str(), str.length());
    socket->write(data);
    socket->flush();
    socket->waitForBytesWritten(2000);
    string buffer = "";
    if (socket->waitForReadyRead(2000)) {
        QByteArray bytes = socket->readAll();
        buffer.append(bytes.toStdString());
        size_t pos = buffer.find('@');
        while (pos == string::npos) {
            if (socket->waitForReadyRead(2000)) {
                QByteArray bytes = socket->readAll();
                buffer.append(bytes.toStdString());
            }
            pos = buffer.find('@');
        }
        string respond = buffer.substr(0, pos - 1);
        r = deserializeRespond(respond);
        if(r.getSessionId() == sessionId) {
            return r;
        }
        else {
            socket->disconnectFromHost();
            serverReboot();
            r.setFunc(FN_ERROR);
            return r;
        }
    }
    socket->disconnectFromHost();
    r.setFunc(FN_ERROR);
    return r;
}

void MainWindow::addNewDino(Dinosaur dino) {
    QVariant var;
    var.setValue(dino);
    ReqRes r = getResp(FN_ADD_DINOSAUR, var);
    if (r.getFunc() == FN_ERROR) return;
    QVariant v = r.getData();
    int id = v.value<int>();
    dino.setId(id);
    QListWidgetItem *item = new QListWidgetItem;
    item->setText(dino.getName());
    QVariant dinoVar;
    dinoVar.setValue(dino);
    item->setData(Qt::UserRole, dinoVar);
    ui->listMain->addItem(item);
    if (ui->fileList->selectedItems().count() > 0)
        ui->saveButton->setEnabled(true);

    socket->disconnectFromHost();
}

void MainWindow::on_addButton_clicked()
{
    AddDialog dialog;
    dialog.setWindowTitle("AddDialog");
    connect(&dialog, SIGNAL(addDino(Dinosaur)), this, SLOT(addNewDino(Dinosaur)));
    dialog.exec();
}

void MainWindow::on_removeButton_clicked()
{
    QList<QListWidgetItem *> selectedItems = ui->listMain->selectedItems();
    QListWidgetItem * item = selectedItems.at(0);
    QVariant dinoVar = item->data(Qt::UserRole);
    Dinosaur dino = dinoVar.value<Dinosaur>();
    QVariant var;
    var.setValue(dino.getId());
    ReqRes r = getResp(FN_DELETE_DINOSAUR, var);
    if (r.getFunc() == FN_ERROR) return;
    QVariant v = r.getData();
    int status = v.value<int>();
    if (status == 1) {
        socket->disconnectFromHost();
        return;
    }
    int row = ui->listMain->row(item);
    QListWidgetItem * i = ui->listMain->takeItem(row);
    delete i;
    if (ui->fileList->selectedItems().count() > 0)
        ui->saveButton->setEnabled(true);
    socket->disconnectFromHost();
    getDinoList();
}

void MainWindow::editDino(Dinosaur dino) {
        QVariant varReq;
        varReq.setValue(dino);
        ReqRes r = getResp(FN_UPDATE_DINOSAUR, varReq);
        if (r.getFunc() == FN_ERROR) return;
        QVariant v = r.getData();
        int status = v.value<int>();
        if (status == 1) {
            socket->disconnectFromHost();
            return;
        }
        QListWidgetItem * item = ui->listMain->selectedItems().at(0);
        int row = ui->listMain->row(item);
        QListWidgetItem * i = ui->listMain->takeItem(row);
                QVariant var = i->data(Qt::UserRole);
                item->setText(dino.getName());
                var.setValue(dino);
                item->setData(Qt::UserRole, var);
                ui->listMain->insertItem(row, item);
                ui->listMain->item(row)->setSelected(true);

                ui->nameEdit->setText(dino.getName());
                ui->ageEdit->setText(QString::number(dino.getAge()) + " years");
                ui->lengthEdit->setText(QString::number(dino.getLength()) + " sm");
                ui->weightEdit->setText(QString::number(dino.getWeight()) + " kg");
                if (ui->fileList->selectedItems().count() > 0)
                    ui->saveButton->setEnabled(true);
        socket->disconnectFromHost();
}

void MainWindow::on_editButton_clicked()
{
    editDialog dialog;
    dialog.setWindowTitle("EditDialog");
    QListWidgetItem * selected = ui->listMain->selectedItems().at(0);
    QVariant var = selected->data(Qt::UserRole);
    Dinosaur dino = var.value<Dinosaur>();
    dialog.setData(dino);
    connect(&dialog, SIGNAL(editDino(Dinosaur)), this, SLOT(editDino(Dinosaur)));
    dialog.exec();
}

void MainWindow::enabledButtons() {
    ui->dinoMenu->setEnabled(true);
    ui->listMain->setEnabled(true);
    ui->addButton->setEnabled(true);
    ui->nameEdit->setEnabled(true);
    ui->lengthEdit->setEnabled(true);
    ui->weightEdit->setEnabled(true);
}

void MainWindow::on_loadButton_clicked()
{
   QList<QListWidgetItem *> selectedItems = ui->fileList->selectedItems();
   QListWidgetItem * selectedItem = selectedItems.at(0);
   QString fileName = selectedItem->text();
   QVariant var;
   var.setValue(fileName);
   ReqRes r = getResp(FN_LOAD_LIST, var);
   if (r.getFunc() == FN_ERROR) return;
   QVariant v = r.getData();
   int status = v.value<int>();
   if (status == 1) {
       socket->disconnectFromHost();
       return;
   }
   socket->disconnectFromHost();
   getDinoList();
   enabledButtons();
}

void MainWindow::on_createNewButton_clicked()
{
    ReqRes r = getResp(FN_NEW_LIST, QVariant());
    if (r.getFunc() == FN_ERROR) return;
    ui->saveButton->setEnabled(false);
    ui->listMain->clear();
    enabledButtons();
    socket->disconnectFromHost();
}

void MainWindow::on_fileList_itemSelectionChanged()
{
    QList<QListWidgetItem *> selectedItems = ui->fileList->selectedItems();
    if (selectedItems.count() > 0) {
        ui->loadButton->setEnabled(true);
        if (ui->listMain->count() > 0)
            ui->saveButton->setEnabled(true);
    }
    else {
        ui->saveButton->setEnabled(false);
        ui->loadButton->setEnabled(false);
    }
}

void MainWindow::on_saveButton_clicked()
{
    QList<QListWidgetItem *> selectedItems = ui->fileList->selectedItems();
    QListWidgetItem * selectedItem = selectedItems.at(0);
    QString fileName = selectedItem->text();
    QVariant var;
    var.setValue(fileName);
    ReqRes r = getResp(FN_SAVE_LIST, var);
    if (r.getFunc() == FN_ERROR) return;
    QVariant v = r.getData();
    int status = v.value<int>();
    if (status == 1) {
        socket->disconnectFromHost();
        return;
    }
    socket->disconnectFromHost();
}
