#ifndef EDITDIALOG_H
#define EDITDIALOG_H

#include <QDialog>
#include "dinosaur.h"

namespace Ui {
class editDialog;
}

class editDialog : public QDialog
{
    Q_OBJECT

public:
    explicit editDialog(QWidget *parent = 0);
    ~editDialog();

private slots:

    void on_buttonBox_accepted();

public slots:

    void setData(Dinosaur dino);

private:
    Ui::editDialog *ui;

signals:
    void editDino(Dinosaur dino);
};

#endif // EDITDIALOG_H
