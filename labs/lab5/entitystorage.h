#ifndef ENTITYSTORAGE_H
#define ENTITYSTORAGE_H

#include <string>
#include <vector>
#include <QtXml>
#include "dinosaur.h"
#include <iostream>
#include <fstream>

    std::vector<Dinosaur> loadXml(std::string fileName);
    void saveXml(std::vector<Dinosaur> & Dinosaurs, std::string fileName);
    std::vector<Dinosaur> loadJson(std::string fileName);
    void saveJson(std::vector<Dinosaur> & Dinosaurs, std::string fileName);


#endif // ENTITYSTORAGE_H
