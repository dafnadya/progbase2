#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "dinosaur.h"
#include <QMessageBox>
#include <QCloseEvent>
#include "entitystorage.h"
#include <QFileDialog>
#include "adddialog.h"
#include "editdialog.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
    void closeEvent(QCloseEvent *event);

private slots:

    void addNewDino(Dinosaur dino);

    void editDino(Dinosaur dino);

    void enableButtons();

    void clearEdits();
    
    void on_listMain_itemSelectionChanged();
    
    void on_addButton_clicked();

    void on_removeButton_clicked();

    void on_editButton_clicked();

    void on_execButton_clicked();

private:
    Ui::MainWindow *ui;

    void createNew();

    void save();

    void load();
};

#endif // MAINWINDOW_H
