#include "adddialog.h"
#include "ui_adddialog.h"

AddDialog::AddDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::AddDialog)
{
    ui->setupUi(this);
}

AddDialog::~AddDialog()
{
    delete ui;
}

void AddDialog::on_okButton_clicked()
{
    //
    Dinosaur dino = Dinosaur();
    dino.setName(ui->nameEl->text());
    dino.setAge(ui->ageEl->value());
    dino.setLength(ui->lengthEl->value());
    dino.setWeight(ui->weightEl->value());
    emit addDino(dino);
    close();
}

void AddDialog::on_nameEl_textEdited(const QString &arg1)
{
    ui->okButton->setEnabled(!ui->nameEl->text().isEmpty());
}

void AddDialog::on_cancelButton_clicked()
{
    close();
}
