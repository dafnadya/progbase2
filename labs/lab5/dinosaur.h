#ifndef DINOSAUR_H
#define DINOSAUR_H

#include <QString>
#include <QMetaType>

class Dinosaur
{
    QString name;
    int age;
    double length;
    double weight;
public:
    Dinosaur() {}
    ~Dinosaur() {}
    Dinosaur(QString name, int age, double length, double weight);

    QString getName();
    int getAge();
    double getLength();
    double getWeight();

    void setName(QString name);
    void setAge(int age);
    void setLength(double length);
    void setWeight(double weight);
};

Q_DECLARE_METATYPE(Dinosaur)

#endif // DINOSAUR_H
