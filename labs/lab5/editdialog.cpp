#include "editdialog.h"
#include "ui_editdialog.h"

editDialog::editDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::editDialog)
{
    ui->setupUi(this);
}

editDialog::~editDialog()
{
    delete ui;
}

void editDialog::setData(Dinosaur dino) {
    ui->nameEdit->setText(dino.getName());
    ui->ageEdit->setValue(dino.getAge());
    ui->lengthEdit->setValue(dino.getLength());
    ui->weightEdit->setValue(dino.getWeight());
}

void editDialog::on_buttonBox_accepted()
{
    Dinosaur dino = Dinosaur();
    dino.setName(ui->nameEdit->text());
    dino.setAge(ui->ageEdit->value());
    dino.setLength(ui->lengthEdit->value());
    dino.setWeight(ui->weightEdit->value());
    emit editDino(dino);
}
