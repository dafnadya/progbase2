#include "entitystorage.h"

using namespace std;

vector<Dinosaur> xmlStringToDinosaurs(string & str) {
    QDomDocument doc;
    vector<Dinosaur> dinosaurs;
    if (!doc.setContent(QString::fromStdString(str))) {
          return dinosaurs;
    }

    QDomElement root = doc.documentElement();

    for (int i = 0; i < root.childNodes().length(); i++) {
        QDomNode dinoNode = root.childNodes().at(i);
        QDomElement dinoEl = dinoNode.toElement();
        Dinosaur d;
        d.setName(dinoEl.attribute("name"));
        d.setAge(dinoEl.attribute("age").toInt());
        d.setLength(dinoEl.attribute("length").toDouble());
        d.setWeight(dinoEl.attribute("weight").toDouble());
        dinosaurs.push_back(d);
    }

    return dinosaurs;
}

string DinosaursToXmlString(vector<Dinosaur> & dinosaurs) {
    vector<Dinosaur> vec = dinosaurs;
    QDomDocument doc;
    QDomElement rootEl = doc.createElement("dinosaurs");
    for(Dinosaur  & d: vec){
        QDomElement dinoEl = doc.createElement("dinosaur");
        dinoEl.setAttribute("name", d.getName());
        dinoEl.setAttribute("age", d.getAge());
        dinoEl.setAttribute("length", d.getLength());
        dinoEl.setAttribute("weight", d.getWeight());
        rootEl.appendChild(dinoEl);
    }
    doc.appendChild(rootEl);
    return doc.toString().toStdString();
}

vector<Dinosaur> loadXml(string fileName) {
    vector<Dinosaur> vec;

    ifstream file(fileName);
    if(!file.is_open()) {
        return vec;
    }
    string str((istreambuf_iterator<char>(file)), istreambuf_iterator<char>());
    vec = xmlStringToDinosaurs(str);
    file.close();
    return vec;
}

void saveXml(vector<Dinosaur> & Dinosaurs, string fileName) {
    ofstream file(fileName);
    string str = DinosaursToXmlString(Dinosaurs);
    file << str;
    file.close();
}

string DinosaursToJsonString(vector<Dinosaur> & Dinosaurs) {
    QJsonDocument doc;
    vector<Dinosaur> vec = Dinosaurs;
    QJsonArray dinosaursArr;

    for(Dinosaur  & l: vec){
        QJsonObject dinosaurObj;
        dinosaurObj.insert("name", l.getName());
        dinosaurObj.insert("age", l.getAge());
        dinosaurObj.insert("length", l.getLength());
        dinosaurObj.insert("weight", l.getWeight());
        dinosaursArr.push_back(dinosaurObj);
    }
    doc.setArray(dinosaursArr);
    return doc.toJson().toStdString();
}

vector<Dinosaur> jsonStringToDinosaurs(string & str) {
    QJsonParseError err;
    QJsonDocument doc = QJsonDocument::fromJson(
           QByteArray::fromStdString(str),
           &err);
    if (err.error != QJsonParseError::NoError) {
        return vector<Dinosaur>();
    }
    vector<Dinosaur> vec;
    QJsonArray DinosaursArr = doc.array();
    for (int i = 0; i < DinosaursArr.size(); i++) {
        Dinosaur lect;
        QJsonValue value = DinosaursArr.at(i);
        QJsonObject lectObj = value.toObject();
        lect.setName(lectObj.value("name").toString());
        lect.setAge(lectObj.value("age").toInt());
        lect.setLength(lectObj.value("length").toDouble());
        lect.setWeight(lectObj.value("weight").toDouble());
        vec.push_back(lect);
     }


    return vec;
}


vector<Dinosaur> loadJson(string fileName){

    vector<Dinosaur> vec;

    ifstream file(fileName);
    if(!file.is_open()) {
        return vec;
    }

    string str((istreambuf_iterator<char>(file)), istreambuf_iterator<char>());
    vec = jsonStringToDinosaurs(str);
    file.close();
    return vec;
}


void saveJson(vector<Dinosaur> & Dinosaurs, string fileName){
    ofstream file(fileName);
    string str = DinosaursToJsonString(Dinosaurs);
    file << str;
    file.close();
}
