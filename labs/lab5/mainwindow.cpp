#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);


    ui->actionCreate_new_2->setShortcuts(QKeySequence::New);
    ui->actionCreate_new_2->setStatusTip(tr("Create a new list"));
    connect(ui->actionCreate_new_2, &QAction::triggered, this, &MainWindow::createNew);

    ui->actionSave_2->setShortcuts(QKeySequence::Save);
    ui->actionSave_2->setStatusTip(tr("Save"));
    connect(ui->actionSave_2, &QAction::triggered, this, &MainWindow::save);

    ui->actionLoad->setShortcuts(QKeySequence::Open);
    ui->actionLoad->setStatusTip(tr("Load from file"));
    connect(ui->actionLoad, &QAction::triggered, this, &MainWindow::load);
}

MainWindow::~MainWindow()
{
    ui->listMain->clear();
    ui->listResult->clear();
    delete ui;
}

void MainWindow::closeEvent(QCloseEvent *event) {
    QMessageBox::StandardButton resBtn = QMessageBox::question(
                this,
                "On close",
                tr("Are you sure?\n"),
                QMessageBox::No | QMessageBox::Yes,
                QMessageBox::No);
    if (resBtn != QMessageBox::Yes) {
        event->ignore();
    }
    else {
        event->accept();
    }
}

std::vector<Dinosaur> loadList(std::string filename) {
    char * str = (char *)filename.c_str();
    while(*str!= '\0' )
            if(*str == '.' && isalnum(*(str+1)))
                break;
            else str++;
    if(strcmp(str, ".xml") == 0)
        return loadXml(filename);
    else if(strcmp(str, ".json") == 0)
        return loadJson(filename);
    else
        return std::vector<Dinosaur>();
}

void saveList(std::vector<Dinosaur> dinos, std::string filename) {
    char * str = (char *)filename.c_str();
    while(*str!= '\0' )
            if(*str == '.' && isalnum(*(str+1)))
                break;
            else str++;
    if(strcmp(str, ".xml") == 0)
        saveXml(dinos, filename);
    else if(strcmp(str, ".json") == 0)
        saveJson(dinos, filename);
}

void MainWindow::enableButtons() {
    ui->addButton->setEnabled(true);
    ui->paramEdit->setEnabled(true);
    ui->execButton->setEnabled(true);
    ui->listMain->setEnabled(true);
    ui->listResult->setEnabled(true);
    ui->paramEdit->setEnabled(true);
}

void MainWindow::load()
{
    QString path = QFileDialog::getOpenFileName(
                this,
                tr("Open File"),
                "../lab5",
                tr("XML JSON files (*.json *.xml)"));
    if (!path.isEmpty()) {
        ui->listMain->clear();
        ui->listResult->clear();
        std::vector<Dinosaur> dinoList = loadList(path.toStdString());
        for (Dinosaur &l : dinoList) {
            QListWidgetItem *item = new QListWidgetItem;
            item->setText(l.getName());
            QVariant var;
            var.setValue(l);
            item->setData(Qt::UserRole, var);
            ui->listMain->addItem(item);
        }

        enableButtons();
    }
}

void MainWindow::createNew()
{
    ui->listMain->clear();
    ui->listResult->clear();
    enableButtons();
}

void MainWindow::clearEdits() {
    ui->nameEdit->setText("");
    ui->ageEdit->setText("");
    ui->lengthEdit->setText("");
    ui->weightEdit->setText("");
}

void MainWindow::on_listMain_itemSelectionChanged()
{
    QList<QListWidgetItem *> selectedItem = ui->listMain->selectedItems();
    if (selectedItem.count() != 0) {
        ui->removeButton->setEnabled(true);
        if (selectedItem.count() == 1) {
            ui->editButton->setEnabled(true);
            QListWidgetItem * selected = selectedItem.at(0);
            QVariant var = selected->data(Qt::UserRole);
            Dinosaur d = var.value<Dinosaur>();
            ui->nameEdit->setText(d.getName());
            ui->ageEdit->setText(QString::number(d.getAge()) + " years");
            ui->lengthEdit->setText(QString::number(d.getLength()) + " sm");
            ui->weightEdit->setText(QString::number(d.getWeight()) + " kg");
        }
        else {
            ui->editButton->setEnabled(false);
            clearEdits();
        }
    }
    else {
        ui->removeButton->setEnabled(false);
        ui->editButton->setEnabled(false);
        clearEdits();
    }
}

void MainWindow::addNewDino(Dinosaur dino) {
    QListWidgetItem *item = new QListWidgetItem;
    item->setText(dino.getName());
    QVariant var;
    var.setValue(dino);
    item->setData(Qt::UserRole, var);
    ui->listMain->addItem(item);
}

void MainWindow::on_addButton_clicked()
{
    AddDialog dialog;
    dialog.setWindowTitle("AddDialog");
    connect(&dialog, SIGNAL(addDino(Dinosaur)), this, SLOT(addNewDino(Dinosaur)));
    dialog.exec();
}

void MainWindow::on_removeButton_clicked()
{
    QList<QListWidgetItem *> selectedItems = ui->listMain->selectedItems();
    for (QListWidgetItem * item : selectedItems) {
        int row = ui->listMain->row(item);
        QListWidgetItem * i = ui->listMain->takeItem(row);
        delete i;
    }
}

void MainWindow::save()
{
    QString path = QFileDialog::getSaveFileName(
                this,
                tr("Save File"),
                "../lab5",
                tr("XML JSON files (*.json *.xml)"));
    if (!path.isEmpty()) {
        std::vector<Dinosaur> vec = std::vector<Dinosaur>();
        for (int i = 0; i < ui->listMain->count(); i++) {
            QListWidgetItem * item = ui->listMain->item(i);
            QVariant var = item->data(Qt::UserRole);
            Dinosaur dino = var.value<Dinosaur>();
            vec.push_back(dino);
        }
        saveList(vec, path.toStdString());
    }
}

void MainWindow::editDino(Dinosaur dino) {
    QListWidgetItem * item = ui->listMain->selectedItems().at(0);
    int row = ui->listMain->row(item);
    QListWidgetItem * i = ui->listMain->takeItem(row);
    QVariant var = i->data(Qt::UserRole);
    item->setText(dino.getName());
    var.setValue(dino);
    item->setData(Qt::UserRole, var);
    ui->listMain->insertItem(row, item);
    ui->listMain->item(row)->setSelected(true);

    ui->nameEdit->setText(dino.getName());
    ui->ageEdit->setText(QString::number(dino.getAge()) + " years");
    ui->lengthEdit->setText(QString::number(dino.getLength()) + " sm");
    ui->weightEdit->setText(QString::number(dino.getWeight()) + " kg");
}

void MainWindow::on_editButton_clicked()
{
    editDialog dialog;
    dialog.setWindowTitle("EditDialog");
    QListWidgetItem * selected = ui->listMain->selectedItems().at(0);
    QVariant var = selected->data(Qt::UserRole);
    Dinosaur dino = var.value<Dinosaur>();
    dialog.setData(dino);
    connect(&dialog, SIGNAL(editDino(Dinosaur)), this, SLOT(editDino(Dinosaur)));
    dialog.exec();
}

void MainWindow::on_execButton_clicked()
{
    ui->listResult->clear();

    double param = ui->paramEdit->value();
    for (int i = 0; i < ui->listMain->count(); i++) {
        QListWidgetItem * item = ui->listMain->item(i);
        QVariant var = item->data(Qt::UserRole);
        Dinosaur dino = var.value<Dinosaur>();

        if (dino.getWeight() < param) {
            QListWidgetItem * newItem = new QListWidgetItem;
            newItem->setText(dino.getName() + " (" + QString::number(dino.getWeight()) + " kg)");
            QVariant newVar;
            newVar.setValue(dino);
            newItem->setData(Qt::UserRole, newVar);
            ui->listResult->addItem(newItem);
        }
    }
}
