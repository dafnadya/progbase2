#include "module.h"
#include <assert.h>

/*
    A set of unique integer values
    Ignore duplicates in operations
*/
struct __Set {
    int * array;
    size_t length;
    size_t capacity;
};

Set * Set_new(void) {
    Set * self = malloc(sizeof(Set));
    self->capacity = 16;
    self->length = 0;
    self->array =  malloc(sizeof(int) * self->capacity);
    return self;
}

Set * Set_newCopy(Set * set) {
    Set * copy = malloc(sizeof(Set));
    copy->capacity = set->capacity;
    copy->length = set->length;
    copy->array =  malloc(sizeof(int) * copy->capacity);
    for (int i = 0; i < copy->length; i++) {
        copy[i] = set[i];
    }
    return copy;
}

void Set_free(Set * self) {
    free(self->array);
    free(self);
}

static void ensureCapacity(Set * self) {
     if (self->length == self->capacity)
    {
        size_t newCapacity = self->capacity * 2;
        void *newArray = realloc(self->array, sizeof(int) * newCapacity);
        if (newArray == NULL)
        {
            assert(0 && "NULL pointer");
        }
        else
        {
            self->array = newArray;
            self->capacity = newCapacity;
        }
    }
}

void Set_insert(Set * self, int value) {
    if (!Set_contains(self, value)) {
        ensureCapacity(self);
        self->array[self->length] = value;
        self->length++;
    }
}

void Set_insertSet(Set * self, Set * other) {
    for (int i = 0; i < other->length; i++) {
        Set_insert(self, other->array[i]);
    }
}

void Set_remove(Set * self, int value) {
    int index = 0;
    while (index < self->length) {
        if (self->array[index] == value) break;
        index++;
    }
    if (index != self->length) {
        for (int i = index + 1; i < self->length; i++)
        {
            self->array[i - 1] = self->array[i];
        }
        self->length--;
    }
}

void Set_removeSet(Set * self, Set * other) {
    for (int i = 0; i < other->length; i++) {
        Set_remove(self, other->array[i]);
    }
}

bool Set_contains(Set * self, int value) {
    for (int i = 0; i < self->length; i++) {
        if (self->array[i] == value) return true;
    }
    return false;
}
bool Set_containsAny(Set * self, int value[], size_t arrayLen) {
    for (int i = 0; i < arrayLen; i++) {
        if (Set_contains(self, value[i])) return true;
    }
    return false;
}

size_t Set_size(Set * self) {
    return self->length;
}
/*
    use qsort() here, ascending order
*/

int cmpfunc (const void * a, const void * b) {
   return ( *(int*)a - *(int*)b );
}

void Set_copyToArraySorted(Set * self, int array[]) {
    for (int i = 0; i < self->length; i++) {
        array[i] = self->array[i];
    }
    qsort(array, self->length, sizeof(int), cmpfunc);
}

// set operations
Set * Set_newUnion(Set * self, Set * other) {
    Set * un = Set_newCopy(self);
    for (int i = 0; i < other->length; i++) {
        if (!Set_contains(un, other->array[i])) {
            Set_insert(un, other->array[i]);
        }
    }
    return un;
}

bool Set_equals(Set * self, Set * other) {
    if (self->length == other->length) {
        for (int i = 0; i < self->length; i++) {
            if (!Set_contains(self, other->array[i])) return false;
        }
        return true;
    }
    return false;
}
/*
    position independent
*/
bool Set_equalsArray(Set * self, int array[], size_t arrayLen) {
    if (self->length == arrayLen) {
        for (int i = 0; i < self->length; i++) {
            if (!Set_contains(self, array[i])) return false;
        }
        return true;
    }
    return false;
}
