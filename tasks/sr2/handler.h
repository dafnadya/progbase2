#pragma once
#include <progbase/events.h>

/*
    handles events from emmiter 
    and sends error event on internal error.
*/

typedef struct __Handler Handler;

Handler * Handler_new(void);
void Handler_free(Handler * self);

void Handler_onEvent(EventHandler * self, Event * event);