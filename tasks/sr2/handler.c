#include "handler.h"
#include "eventtypes.h"
#include <stdlib.h>
#include <assert.h>

typedef struct __Stack {
    int * nums;
    int top;
} Stack;

Stack * Stack_new() {
    Stack * self = malloc(sizeof(Stack));
    self->nums = malloc(sizeof(int) * 24);
    self->top = -1;
    return self;
}

void Stack_free(Stack * self) {
    free(self->nums);
    free(self);
}

int pop(Stack * st) {
    st->top--;
    return st->nums[st->top];
}

void push(Stack * st, int x) {
    st->top++;
    st->nums[st->top] = x;
}

struct Handler {
    Stack * st;
};

Handler * Handler_new(void) {
    Handler * self = malloc(sizeof(Handler));
    self->st = Stack_new();
    return self;
}
void Handler_free(Handler * self) {
    Stack_free(self->st);
    free(self);
}

void Handler_onEvent(EventHandler * self, Event * event) {
    Handler * handler = self->data;
    switch (event->type) {
        case EmitterCommandEventId: {
            Command * com = (Command *)event->data;
            switch (com->operation) {
                case OpNone: {
                    int value = com->value;
                    push(handler->st, value);
                    break;
                }
                case OpSum: {
                    if (handler->st->top < 1)
                        EventSystem_emit(Event_new(self, HandlerErrorResponseEventId, NULL, NULL));
                    else {
                        int first = pop(handler->st);
                        int second = pop(handler->st);
                        push(handler->st, first + second);
                    }
                    break;
                }
                case OpMult: {
                    if (handler->st->top < 1)
                        EventSystem_emit(Event_new(self, HandlerErrorResponseEventId, NULL, NULL));
                    else {
                        int first = pop(handler->st);
                        int second = pop(handler->st);
                        push(handler->st, first * second);
                    }
                    break;
                }
                case OpDiv: {
                    if (handler->st->top < 0)
                        EventSystem_emit(Event_new(self, HandlerErrorResponseEventId, NULL, NULL));
                    else {
                        int first = pop(handler->st);
                        int second = pop(handler->st);
                        if (second == 0) {
                            EventSystem_emit(Event_new(self, HandlerErrorResponseEventId, NULL, NULL));
                        }
                        else {
                            push(handler->st, first / second);
                        }
                    }
                    break;
                }
                default: {
                    EventSystem_emit(Event_new(self, HandlerErrorResponseEventId, NULL, NULL));
                    break;
                }
            }
            break;
        }
        case EmitterResultRequestEventId: {
            Handler * handler = self->data;
            if (handler->st->top < 0)
                EventSystem_emit(Event_new(self, HandlerErrorResponseEventId, NULL, NULL));
            else {
                int * top = handler->st->nums[handler->st->top];
                EventSystem_emit(Event_new(self, HandlerResultResponseEventId, top, NULL));
            }
            break;
        }
    }
}