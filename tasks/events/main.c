#include <stdlib.h>
#include <stdio.h>
#include <progbase/events.h>
#include <progbase/console.h>
#include <string.h>

enum
{
    StringInputEventTypeId,
    StringOutputEventTypeId,
};

typedef struct StringBuffer StringBuffer;

struct StringBuffer
{
    char *buffer;
    size_t capacity;
    size_t length;
};

StringBuffer *StringBuffer_new(void);
void StringBuffer_free(StringBuffer *self);
void StringBuffer_appendChar(StringBuffer *self, char ch);
void StringBuffer_clear(StringBuffer *self);

void StringInputHandler_update(EventHandler *self, Event *event);
void StringInputListener_update(EventHandler *self, Event *event);

static void checkNULL(void *toCheck)
{
    if (toCheck == NULL)
    {
        fwrite("Out of memory", 1, 14, stderr);
        abort();
    }
}

int main(void)
{
    EventSystem_init();

    EventSystem_addHandler(EventHandler_new(NULL, NULL, StringInputListener_update));
    EventSystem_addHandler(
        EventHandler_new(
            StringBuffer_new(),
            (DestructorFunction)StringBuffer_free,
            StringInputHandler_update));

    Console_clear();

    EventSystem_loop();

    EventSystem_cleanup();
    return 0;
}

void StringInputListener_update(EventHandler *self, Event *event) {
    if (conIsKeyDown())
    {
        char keyCode = getchar();
        if (keyCode == 27)
        {
            puts("");
            EventSystem_exit();
        }
        else
        {
            if (keyCode == 10)
            {
                EventSystem_emit(Event_new(self, StringOutputEventTypeId, NULL, NULL));
            }
            else
            {
                char *cp = malloc(sizeof(char));
                *cp = keyCode;
                EventSystem_emit(Event_new(self, StringInputEventTypeId, cp, free));
            }
        }
    }
}

void StringInputHandler_update(EventHandler *self, Event *event)
{
    switch (event->type)
    {
    case StartEventTypeId:
    {
        puts("Create handler that can be in two states: "
             "getting input of the string and outputing the string.\n"
             "In inputing state previously entered string have to be cleaned "
             "and new input is being written to new string.\n");
        Console_setCursorAttribute(FG_CYAN);
        puts("Press [Enter] to switch state.\n"
             "Press [Esc] to exit.\n");
        Console_setCursorAttribute(FG_INTENSITY_RED);
        printf("*input state* ");
        Console_setCursorAttribute(FG_DEFAULT);
        printf("Enter string: ");
        break;
    }
    case StringInputEventTypeId:
    {
        StringBuffer *string = (StringBuffer *)self->data;
        char keyCode = *(char *)event->data;
        StringBuffer_appendChar(string, keyCode);
        break;
    }
    case StringOutputEventTypeId:
    {
        StringBuffer *string = (StringBuffer *)self->data;
        Console_setCursorAttribute(FG_INTENSITY_RED);
        printf("*output state* ");
        Console_setCursorAttribute(FG_DEFAULT);
        printf("Your string: %s\n", string->buffer);
        StringBuffer_clear(string);
        Console_lockInput();
        char key = getchar();
        while (key != 27 && key != 10)
        {
            key = getchar();
        }
        if (key == 27)
        {
            getchar();
            EventSystem_exit();
        }
        else
        {
            Console_setCursorAttribute(FG_INTENSITY_RED);
            printf("\n*input state* ");
            Console_setCursorAttribute(FG_DEFAULT);
            printf("Enter string: ");
        }
        Console_unlockInput();
        break;
    }
    }
}

StringBuffer *StringBuffer_new(void)
{
    StringBuffer *self = malloc(sizeof(StringBuffer));
    checkNULL(self);
    self->capacity = 256;
    self->buffer = malloc(sizeof(char) * self->capacity);
    checkNULL(self->buffer);
    self->buffer[0] = '\0';
    self->length = 1;
    return self;
}

void StringBuffer_free(StringBuffer *self)
{
    checkNULL(self);
    free(self->buffer);
    free(self);
}

static void ensureCapacity(StringBuffer *self, int appendLength)
{
    if (self->length + appendLength > self->capacity)
    {
        size_t newCapacity = self->capacity * 2;
        char *newBuffer = realloc(self->buffer, sizeof(char) * newCapacity);
        checkNULL(newBuffer);
        self->buffer = newBuffer;
        self->capacity = newCapacity;
    }
}

void StringBuffer_appendChar(StringBuffer *self, char ch)
{
    checkNULL(self);
    ensureCapacity(self, 1);
    self->buffer[self->length] = '\0';
    self->buffer[self->length - 1] = ch;
    self->length++;
}

void StringBuffer_clear(StringBuffer *self)
{
    checkNULL(self);
    self->buffer[0] = '\0';
    self->length = 1;
}