#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <ctype.h>

typedef struct List
{
    int *items;
    size_t length;
    size_t capacity;
}List;

int cmpfunc (const void * a, const void * b) {
   return ( *(int*)a - *(int*)b );
}

int main(void) {
    /*char ascii;
    puts("Enter char");
    scanf("%c", &ascii);
    printf("%i", ascii);
    puts("");
    return 0;*/ 

    
    List *self = malloc(sizeof(List));
    self->capacity = 256;
    self->items = malloc(sizeof(int) * self->capacity);
    self->length = 0;

    char string[] = "12 32   22 34";
    char buffer[100];
    for (int i = 0, k = 0; i < strlen(string); i++) {
        if (isdigit(string[i])) {
            while (isdigit(string[i])) {
                buffer[k] = string[i];
                i++;
                k++;
            }
            self->items[self->length] = atoi(buffer);
            self->length++;
            buffer[0] = '\0';
            k = 0;
        }
    }

    qsort(self->items, self->length, sizeof(int), cmpfunc);


    for (int i = 0; i < self->length; i++) {
        printf("%i ", self->items[i]);
    }
    return 0;
}