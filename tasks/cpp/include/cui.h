#pragma once 

#include <vector>
#include <circle.h>

void printMenu();
Circle *getCircle();
void printCircles(vector<Circle *> circles);
void printWantedCircles(vector<Circle *> circles, int len);
int getLenFromInput();