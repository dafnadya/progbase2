#pragma once

#include <string>

class Center {
    protected:
        int x;
        int y;
};

using namespace std;

class Circle: Center {
    string name;
    int radius;

    public:
    Circle();
    Circle(string name, int radius, int x, int y);

    string getName();
    int getRadius();
    int getX();
    int getY();

    double countLength();
};