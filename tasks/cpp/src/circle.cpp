#include <circle.h>
#include <cmath>

Circle::Circle(string name, int radius, int x, int y) {
    this->name = name;
    this->radius = radius;
    this->x = x;
    this->y = y;
}

string Circle::getName() {
    return name;
}

int Circle::getRadius() {
    return radius;
}

int Circle::getX() {
    return x;
}

int Circle::getY() {
    return y;
}

double Circle::countLength() {
    return 2 * radius * M_PI;
}