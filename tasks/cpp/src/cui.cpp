#include <cui.h>
#include <iostream>
#include <limits>
#include <progbase/console.h>

using namespace std;

void printMenu() {
    Console_setCursorPosition(0, 0);
    cout << "Choose option (Enter 1, 2, 3 or 4):" << endl;
    cout << "1. Print collection" << endl;
    cout << "2. Add new element" << endl;
    cout << "3. Print circles with length less than x" << endl;
    cout << "4. Exit" << endl;
}

int getCoordinate() {
    int c;
    while (true) {
        cin >> c;
        if (cin.good() == true) break;
        cin.clear();
        cin.ignore(numeric_limits<streamsize>::max(), '\n');
        cout << "wrong input! try again:" << endl;
    }
    return c;
}

Circle *getCircle() {
    std::string name;
    cout << "Enter name:" << endl; 
    cin.clear();
    cin.ignore(numeric_limits<streamsize>::max(), '\n');
    getline(cin, name);
    cout << "Enter radius: (>= 0)" << endl;
    int radius = getLenFromInput();
    cout << "Enter center coordinates:\nx: ";
    int x = getCoordinate();
    cout << "y: ";
    int y = getCoordinate();
    return new Circle(name, radius, x, y);
}

void printCircle(Circle * circle, int index) {
    Console_setCursorAttribute(FG_CYAN);
    cout << "Circle # " << index << endl;
    Console_setCursorAttribute(FG_DEFAULT);
    cout << "Name: " << circle->getName() << endl;
    cout << "Radius: " << circle->getRadius() << endl;
    cout << "Center: " << "(" << circle->getX() << "; " << circle->getY() << ")" << endl;
    cout << endl;
}

void printCircles(vector<Circle *> circles) {
    Console_clear();
    Console_setCursorAttribute(FG_RED);
    cout << "*press smth to continue*\n" << endl;
    Console_setCursorAttribute(FG_DEFAULT);
    int index = 0;
    for (auto& i : circles) {
        printCircle(i, index);
        index++;
    }
    if (index == 0)
        cout << "No circles" << endl;
}

void printWantedCircles(vector<Circle *> circles, int len) {
    Console_clear();
    Console_setCursorAttribute(FG_INTENSITY_MAGENTA);
    cout << "*press smth to continue*\n" << endl;
    Console_setCursorAttribute(FG_DEFAULT);
    int index = 0;
    bool isAny = false;
    for (auto& i : circles) { 
        if (i->countLength() < len) {
            printCircle(i, index);
            isAny = true;
        }
        index++;
    }
    if (!isAny)
        cout << "No circles with length less than " << len << endl;
}

int getLenFromInput() {
    int len;
    while (true) {
        cin >> len;
        if (cin.good() == true && len >= 0) break;
        cin.clear();
        cin.ignore(numeric_limits<streamsize>::max(), '\n');
        cout << "wrong input! try again:" << endl;
    }
    return len;
}