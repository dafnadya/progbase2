#include <vector>
#include <circle.h>
#include <iostream>
#include <progbase/console.h>
#include <cui.h>
#include <limits>

int main(void) {
    vector<Circle *> circles;
    Console_showCursor();
    while (true) {
        Console_clear();
        printMenu();
        int option;
        cin >> option;
        switch (option) {
            case 1: { 
                printCircles(circles);
                Console_getChar();
                break;
            }
            case 2: {
                circles.push_back(getCircle());
                break;
            }
            case 3: {
                Console_clear();
                cout << "Enter length: (>= 0)" << endl;
                int len = getLenFromInput();
                printWantedCircles(circles, len);
                Console_getChar();
                break;
            }
            case 4: {
                 for (auto& i : circles) {
                     delete i;
                 }
                return 0;
            }
            default: {
                cin.clear();
                cin.ignore(numeric_limits<streamsize>::max(), '\n');
                break;
            }

        }
    }
     for (auto& i : circles) {
         delete i;
    }
    return 0;
}