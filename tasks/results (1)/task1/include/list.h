//#pragma once
//
//#include <stdio.h>
//#include <string.h>
//#include <ctype.h>
//#include <assert.h>
//#include <stdbool.h>
//#include <stdlib.h>
//
//
//typedef struct _List List;
//typedef struct _SLNode SLNode;
//
//
//List * List_new(void);
//void List_free(List * self);
//void List_clear(List * self);
//
//void List_insert(List * self, void * value, size_t index);
//void List_add(List * self, void * value);
//void * List_at(List * self, size_t index);
//void * List_set(List * self, size_t index, void * value);
//void * List_removeAt(List * self, size_t index);
//size_t List_count(List * self);

#pragma once

#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <assert.h>
#include <stdbool.h>
#include <stdlib.h>



typedef struct _List List;
typedef struct _SLNode SLNode;


List * List_new(void);
void List_free(List * self);
void List_clear(List * self);

void List_addFirst(List * self, void * value);
void List_insert(List * self, void * value, size_t index);
void List_add(List * self, void * value);

void * List_at(List * self, size_t index);
void * List_set(List * self, size_t index, void * value);

void * List_removeAt(List * self, size_t index);
size_t List_count(List * self);

bool List_containsInteger(List * self, int data);