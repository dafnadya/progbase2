#pragma once

#include <list.h>


int getMin(List * self);

int getMax(List * self);

double getArytm(List * self);

List * createUniqueList(List * self);