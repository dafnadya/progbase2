#include "numList_ext.h"


int getMin(List * self) {
	assert(self != NULL && "Error");
	int min = *(int*)List_at(self, 0);
	for(int i = 1; i < List_count(self); i++) {
		if(*(int*)List_at(self, i) < min) {
			min = *(int*)List_at(self, i);
		}
	}
	return min;
}

int getMax(List * self) {
	assert(self != NULL && "Error");
	int max = *(int*)List_at(self, 0);
	for(int i = 1; i < List_count(self); i++) {
		if(*(int*)List_at(self, i) > max) {
			max = *(int*)List_at(self, i);
		}
	}
	return max;
}

double getArytm(List * self) {
	assert(self != NULL && "Error");
	double sum = 0;
	for(int i = 0; i < List_count(self); i++) {
		sum += *(int*)List_at(self, i);
	}
	return sum / List_count(self);
}

List * createUniqueList(List * self) {
	assert(self != NULL && "Error");
	List * list = List_new();
	for(int i = 0; i < List_count(self); i++) {
		int a = *(int*)List_at(self, i);
		if(!List_containsInteger(list, a)) {
			int * pa = (int *)malloc(sizeof(int));
			*pa = a;
			List_add(list, pa);
		}
	}

	return list;
}