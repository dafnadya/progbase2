#include <list.h>

struct _SLNode {
	void * value;
	SLNode * next;
};

struct _List {
	SLNode * head;
	int size;
};


SLNode * SLNode_new(void * data) {

	SLNode * self = malloc (sizeof(SLNode));
	if (self == NULL) {
		fprintf(stderr, "Out of memory\n");
		abort();
	}
	self->next = NULL;
	self->value = data;
	return self;
}

List * List_new (void) {
	List * self = malloc (sizeof(List));
	if (self == NULL) {
		fprintf(stderr, "Out of memory\n");
		abort();
	}
	self->head = NULL;
	self->size = 0;
	return self;
}

void SLNode_free(SLNode * self) {
	assert(self != NULL && "Free null pointer");
	free (self);
}

void List_free(List * self) {
	assert(self != NULL && "Free null list");
	free (self);
}

void List_clear(List * self) {
	assert(self != NULL && "Free null pointer");
	SLNode * cur = self->head;
	while (cur != NULL) {
		SLNode * tmp = cur->next;
		SLNode_free(cur);
		cur = tmp;
	}
	List_free(self);
}

void List_addFirst(List * self, void * value) {
	if (self == NULL) {
		assert(self != NULL && "List not found");
		fprintf(stderr, "List not found\n");
		return;
	}
	SLNode * node = SLNode_new(value);
	node->next = self->head;
	self->head = node;
	self->size++;
}

void List_add(List * self, void * value) {
	SLNode * node = SLNode_new(value);
	if(self->head == NULL) {
		self->head = node;
		self->size++;
		return;
	}
	SLNode * cur = self->head;
	while(cur->next != NULL) {
		cur = cur->next;
	}
	cur->next = node;
	self->size++;
}


void List_insert(List * self, void * value, size_t index) {
	assert(self != NULL && "List not found");
	if (index == 0 || self->head == NULL) {
		List_addFirst(self, value);
		return;
	}
	SLNode * cur = self->head;
	int i = 0;
	while (i != index - 1 && cur->next != NULL) {
		i++;
		cur = cur->next;
	}
	assert(i == index - 1 && "Invalid index");
	SLNode * node = SLNode_new(value);
	node->next = cur->next;
	cur->next = node;
	self->size++;
}

static void * List_removeFirst(List * self) {
	SLNode * oldNode = self->head;
	void * oldValue = oldNode->value;
	self->head = oldNode->next;
	SLNode_free(oldNode);
	self->size--;
	return oldValue;
}

void * List_removeAt(List * self, size_t index) {

	assert(self != NULL && "Removing from empty list");
	int i = 0;
	if (index == 0) {
		return List_removeFirst(self);
	}
	SLNode * cur = self->head;
	while (i != index - 1 && cur->next->next != NULL) {
		i++;
		cur = cur->next;
	}
	if (i != index - 1) {
		assert(i == index - 1 && "Invalid index");
		fprintf (stderr, "Invalid index: %zu\n", index);
		return NULL;
	}
	SLNode * oldNode = cur->next;
	void * oldValue  = oldNode->value;
	cur->next = oldNode->next;
	SLNode_free(oldNode);
	self->size--;
	return oldValue ;
}

size_t List_count(List * self) {
	if(self == NULL)
		return 0;
	return self->size;
}

void * List_at(List * self, size_t index) {

	if (self == NULL) {
		assert(self != NULL && "Taking element from empty list");
		return NULL;
	}
	int i = 0;
	SLNode * cur = self->head;
	while (i != index && cur != NULL) {
		i++;
		cur = cur->next;
	}
	assert(i == index && "Invalid index");
	return cur->value;
}

void * List_set(List * self, size_t index, void * value) {

	if (self == NULL) {
		assert(self != NULL && "Rewriting empty list");
		fprintf(stderr, "Rewriting empty list");
		return NULL;
	}
	int i = 0;
	SLNode * cur = self->head;
	while (i != index && cur->next != NULL) {
		i++;
		cur = cur->next;
	}
	if (i != index) {
		assert(i == index && "Invalid index");
		fprintf(stderr, "Invalid index");
		return NULL;
	}
	void * answer = cur->value;
	cur->value = value;
	return answer;
}

bool List_containsInteger(List * self, int data) {
	for(int i = 0; i < List_count(self); i++) {
		if(*(int*)List_at(self, i) == data) return true;
	}
	return false;
}
/*
 *
 *
 *
 *
 */

//#include <list.h>
//
//struct _List {
//	SLNode * head;
//	size_t size;
//};
//
//struct _SLNode {
//	void * value;
//	SLNode * next;
//};
//
//List * List_new(void) {
//	List * self = malloc(sizeof(List));
//	if(self == NULL) {
//		fprintf(stderr, "Out of memory");
//		abort();
//	}
//	self->head = NULL;
//	self->size = 0;
//	return self;
//}
//SLNode * SLNode_new(void * value) {
//	SLNode * self = malloc(sizeof(SLNode));
//	if(self == NULL) abort();
//	self->value = value;
//	self->next = NULL;
//}
//void List_free(List * self) {
//	if(self == NULL) abort();
//		free(self);
//}
//void SLNode_free(SLNode * self) {
//	if(self == NULL) abort();
//	free(self);
//}
//void List_clear(List * self) {
//	if(self == NULL) abort();
//	SLNode * cur = self->head;
//	while(cur != NULL) {
//		SLNode * tmp = cur->next;
//		SLNode_free(cur);
//		cur = tmp;
//	}
//	List_free(self);
//}
//
//void List_insert(List * self, void * value, size_t index) {
//	if(index < 0 || index > List_count(self) || self == NULL) {
//		abort();
//	}
//	if(index == 0) {
//		List_addFirst(self, value);
//		return;
//	} else if(index == List_count(self)) {
//		List_add(self, value);
//		return;
//	}
//	SLNode * newNode = SLNode_new(value);
//	SLNode * cur = self->head;
//	for(int i = 1; i < index; i++)
//		cur = cur->next;
//	newNode->next = cur->next;
//	cur->next = newNode;
//	self->size++;
//
//}
//void List_addFirst(List * self, void * value) {
//	if(self == NULL) abort();
//	SLNode * node = SLNode_new(value);
//	node->next = self->head;
//	self->head = node;
//	self->size++;
//}
//void List_add(List * self, void * value) {
//	if(self == NULL)
//		abort();
//	SLNode * newNode = SLNode_new(value);
//	if(self->head == NULL) {
//		self->head = newNode;
//		self->size++;
//		return;
//	}
//	SLNode * cur = self->head;
//	while(cur->next != NULL)
//		cur = cur->next;
//	cur->next = newNode;
//	self->size++;
//}
//void * List_at(List * self, size_t index) {
//	if(self == NULL || index < 0 || index > List_count(self)) abort();
//	SLNode * cur = self->head;
//	for(int i = 0; i < index; i++) {
//		cur = cur->next;
//	}
//	return cur->value;
//}
//void * List_set(List * self, size_t index, void * value) {
//	if(self == NULL || index < 0 || index > List_count(self)) abort();
//	SLNode * cur = self->head;
//	for(int i = 0; i < index; i++) {
//		cur = cur->next;
//	}
//	void * oldValue = cur->value;
//	cur->value = value;
//	return oldValue;
//}
//void * List_removeFirst(List * self) {
//	if(self == NULL || self->head == NULL) abort();
//	void * oldValue = self->head;
//	SLNode * oldNode = self->head;
//	self->head = oldNode->next;
//	SLNode_free(oldNode);
//	return  oldValue;
//}
//void * List_removeAt(List * self, size_t index) {
//	if(self == NULL || index < 0 || index > List_count(self))
//		abort();
//	if(index == 0)
//		return List_removeFirst(self);
//	SLNode * cur = self->head;
//	int i;
//	for(i = 1; i < index; i++) {
//		cur = cur->next;
//	}
//	if(i != index - 1)
//		abort();
//	void * oldValue = cur->next->value;
//	SLNode * oldNode = cur->next;
//	cur->next = oldNode->next;
//	SLNode_free(oldNode);
//
//	self->size--;
//	return oldValue;
//}
//
//size_t List_count(List * self) {
//	return self->size;
//}
