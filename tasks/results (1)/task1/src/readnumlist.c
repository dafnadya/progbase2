#include "readnumlist.h"

static bool isCorrectFilePath(char * filePath) {
	FILE * f = fopen(filePath, "r");
	if(f == NULL) return false; // error
	return true;
}


char * _strdup_ (const char *s) { // ERRORS WITH STRDUP((
	char *d = malloc (strlen (s) + 1);   // Space for length plus nul
	if (d == NULL) return NULL;          // No memory
	strcpy (d,s);                        // Copy the characters
	return d;                            // Return the new string
}

char * strFromFile(char * filePath) {
	// if(!isCorrectFilePath()) return NULL;
	FILE * f = fopen(filePath, "r");
	if(f == NULL) return NULL; // error
	fseek(f, 0, SEEK_END);
	long size = ftell(f);
	fseek(f, 0, SEEK_SET); // return to begin of file
	char str[size + 1]; // for '\0'
	fread(str, 1, size, f);
	fclose(f);
	str[size] = '\0';
	return _strdup_(str);
}

List * numListFromStr(char * filePath) {
	char * str = strFromFile(filePath);
	List * numList = List_new();
	char * copy = str;
	while(copy != '\0') {
		int ret = strtol(copy, &copy, 10);\
		if(ret == 0) break;
		int * pret = (int *)malloc(sizeof(int));
		*pret = ret;
		List_add(numList, pret);
	}

	free(str);
	return numList;
}
















