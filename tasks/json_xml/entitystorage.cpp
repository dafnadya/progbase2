#include <iostream>
#include <fstream>
#include "entitystorage.h"

using namespace std;

vector<Lecturer> xmlStringToLecturers(string & str) {
    QDomDocument doc;
    vector<Lecturer> lecturers;
    if (!doc.setContent(QString::fromStdString(str))) {
          cerr << "error parsing xml!" << endl;
          return lecturers;
    }

    QDomElement root = doc.documentElement();

    for (int i = 0; i < root.childNodes().length(); i++) {
        QDomNode lecturerNode = root.childNodes().at(i);
        QDomElement lecturerEl = lecturerNode.toElement();
        Lecturer l;
        l.setName(lecturerEl.attribute("name"));
        l.setExpr(lecturerEl.attribute("expr").toInt());
        l.setRating(lecturerEl.attribute("rating").toDouble());
        for (int i = 0; i < lecturerEl.childNodes().length(); i++) {
              QDomNode groupNode = lecturerEl.childNodes().at(i);
              QDomElement groupEl = groupNode.toElement();
              Group g;
              g.setGroupName(groupEl.attribute("groupName"));
              l.addGroup(g);
        }
        lecturers.push_back(l);
    }

    return lecturers;
}

string lecturersToXmlString(vector<Lecturer> & lecturers) {
    vector<Lecturer> vec = lecturers;
    QDomDocument doc;
    QDomElement rootEl = doc.createElement("lecturers");
    for(Lecturer  & l: vec){
        QDomElement lecturerEl = doc.createElement("lecturer");
        lecturerEl.setAttribute("name", l.getName());
        lecturerEl.setAttribute("expr", l.getExpr());
        lecturerEl.setAttribute("rating", l.getRating());
        for(Group &  g: l.getGroups()){
            QDomElement groupEl = doc.createElement("group");
            groupEl.setAttribute("groupName", g.getGroupName());
            lecturerEl.appendChild(groupEl);
        }
    rootEl.appendChild(lecturerEl);
    }
    doc.appendChild(rootEl);
    return doc.toString().toStdString();
}

vector<Lecturer> XmlEntityStorage::load() {
    vector<Lecturer> vec;

    ifstream file(this->_name);
    if(!file.is_open()) {
        string err = "No such file";
        throw MessageException(err);
        return vec;
    }

    cout << endl << "* file name: " << this->_name << " *" << endl;

    string str((istreambuf_iterator<char>(file)), istreambuf_iterator<char>());
    vec = xmlStringToLecturers(str);
    file.close();
    return vec;
}

void XmlEntityStorage::save(vector<Lecturer> & lecturers) {
    ofstream file(this->_name);
    string str = lecturersToXmlString(lecturers);
    file << str;
    file.close();
}

string lecturersToJsonString(vector<Lecturer> & lecturers) {
    QJsonDocument doc;
    vector<Lecturer> vec = lecturers;
    QJsonArray lecturersArr;

    for(Lecturer  & l: vec){
        QJsonObject lecturerObj;
        lecturerObj.insert("name", l.getName());
        lecturerObj.insert("expr", l.getExpr());
        lecturerObj.insert("rating", l.getRating());
        QJsonArray groupsArr;
        for (Group & s: l.getGroups()) {
            QJsonObject gr;
            gr.insert("groupName", s.getGroupName());
            groupsArr.append(gr);
        }
        lecturerObj.insert("groups", groupsArr);
        lecturersArr.push_back(lecturerObj);
    }
    doc.setArray(lecturersArr);
    return doc.toJson().toStdString();
}

vector<Lecturer> jsonStringToLecturers(string & str) {
    QJsonParseError err;
    QJsonDocument doc = QJsonDocument::fromJson(
           QByteArray::fromStdString(str),
           &err);
    if (err.error != QJsonParseError::NoError) {
        string err = "Invalid json format";
        throw MessageException(err);
        return vector<Lecturer>();
    }
    vector<Lecturer> vec;
    QJsonArray lecturersArr = doc.array();
    for (int i = 0; i < lecturersArr.size(); i++) {
        Lecturer lect;
        QJsonValue value = lecturersArr.at(i);
        QJsonObject lectObj = value.toObject();
        lect.setName(lectObj.value("name").toString());
        lect.setExpr(lectObj.value("expr").toInt());
        lect.setRating(lectObj.value("rating").toDouble());
        QJsonArray groupsArr = lectObj.value("groups").toArray();
        for (int j = 0; j < groupsArr.size(); j++) {
              QJsonValue value = groupsArr.at(j);
              QJsonObject groupObj = value.toObject();
              lect.addGroup(groupObj.value("groupName").toString());
        }
        vec.push_back(lect);
     }


    return vec;
}


vector<Lecturer> JsonEntityStorage::load(){

    vector<Lecturer> vec;

    ifstream file(this->_name);
    if(!file.is_open()) {
        string err = "No such file";
        throw MessageException(err);
        return vec;
    }

    cout << endl << "* file name: " << this->_name << " *" << endl;

    string str((istreambuf_iterator<char>(file)), istreambuf_iterator<char>());
    vec = jsonStringToLecturers(str);
    file.close();
    return vec;
}


void JsonEntityStorage::save(vector<Lecturer> & lecturers){
    ofstream file(this->_name);
    string str = lecturersToJsonString(lecturers);
    file << str;
    file.close();
}
