#ifndef STORAGEMANAGER_H
#define STORAGEMANAGER_H


#include "entitystorage.h"

class StorageManager {
public:
    static EntityStorage * createStorage(std::string storageFileName);
};
#endif // STORAGEMANAGER_H
