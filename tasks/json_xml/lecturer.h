#ifndef MANAGER_H
#define MANAGER_H

#include <string>
#include <list>
#include <QDebug>
#include <iostream>
#include <QtXml>

using namespace std;

class Group {
public:
    QString groupName;

    Group() {}
    Group(QString name);
    ~Group() {}
    void setGroupName(QString name);
    QString getGroupName();
    void printGroup();
};

class Lecturer: public Group {
public:
    QString name;
    int expr; //experience
    double rating;
    vector<Group> groups;

    Lecturer() {}
    Lecturer(QString name, int expr, double rating, vector<Group> & groups);
    ~Lecturer() {}
    void setName(QString name);
    QString getName();
    void setExpr(int expr);
    int getExpr();
    void setRating(double rating);
    double getRating();
    void addGroup(Group group);
    void setGroups(vector<Group> & groups);
    vector<Group> & getGroups();
    void printLecturer();
};

#endif // MANAGER_H
