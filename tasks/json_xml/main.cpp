#include <QCoreApplication>
#include "lecturer.h"
#include <iostream>
#include <QtXml>
#include "storagemanager.h"

using namespace std;

void printVector(vector<Lecturer> vec);

int main(void)
{
    vector<Group> g1;
    g1.push_back(Group("kp-72"));
    g1.push_back(Group("kp-73"));
    vector<Group> g2;
    g2.push_back(Group("km-72"));
    g2.push_back(Group("km-71"));
    Lecturer l1 ("RA", 3, 5.0, g1);
    Lecturer l2 ("Suchuck", 10, 4.0, g2);
    Lecturer l3 ("Petrovich", 22, 4.5, g1);

    vector<Lecturer> vec;
    vec.push_back(l1);
    vec.push_back(l2);
    vec.push_back(l3);

    vector<EntityStorage *> storages;

    try {
        storages.push_back(StorageManager::createStorage("data.json"));
    } catch (const MessageException &e){
        cerr << e.what() << endl;
    }
    try {
        storages.push_back(StorageManager::createStorage("data.xml"));
    } catch (const MessageException &e){
        cerr << e.what() << endl;
    }
    try {
        storages.push_back(StorageManager::createStorage("data.fake"));
    } catch(const MessageException &e){
        cerr << e.what() << endl;
    }

    try {
        for(auto &l: storages)
            l->save(vec);
    } catch (const MessageException &e) {
        cerr << e.what() << endl;
    }


    cout << "Storages were saved. Press smth to continue" << endl;
    cin.get();

    try {
        for(auto &l: storages) {
            vec = l->load();
            printVector(vec);
        }
    } catch (const MessageException &e) {
        cerr << e.what() << endl;
    }

    cout << "Load complete" << endl;

    for(auto &l: storages)
        delete l;

    return 0;
}

void printVector(vector<Lecturer> vec) {
    for(Lecturer &l: vec)
        l.printLecturer();
}
