#include "storagemanager.h"
#include <cctype>

EntityStorage * StorageManager::createStorage(std::string storageFileName){
    char * str = (char *)storageFileName.c_str();
    while(*str!= '\0' )
            if(*str == '.' && isalnum(*(str+1)))
                break;
            else str++;
    if(strcmp(str, ".xml") == 0)
        return (EntityStorage *)(new XmlEntityStorage(storageFileName));
    else if(strcmp(str, ".json") == 0)
        return (EntityStorage *)(new JsonEntityStorage(storageFileName));
    else {
        string err = "Wrong extension: ";
        err.append(str);
        throw MessageException(err);
        return nullptr;
    }

}
