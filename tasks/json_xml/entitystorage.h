#ifndef ENTITYSTORAGE_H
#define ENTITYSTORAGE_H

#include <string>
#include <vector>
#include "lecturer.h"
#include "messageexc.h"

class EntityStorage {

protected:
    std::string _name;
    EntityStorage(std::string & name) { this->_name = name; }
public:
    std::string & name() { return this->_name; }

    virtual std::vector<Lecturer> load() = 0;
    virtual void save(std::vector<Lecturer> & lecturers) = 0;
};

class XmlEntityStorage: public EntityStorage {
public:
    XmlEntityStorage(std::string name) : EntityStorage(name){}
    std::vector<Lecturer> load();
    void save(std::vector<Lecturer> & lecturers);
};

class JsonEntityStorage: public EntityStorage {
public:
    JsonEntityStorage(std::string name) : EntityStorage(name){}
    std::vector<Lecturer> load();
    void save(std::vector<Lecturer> & lecturers);
};


#endif // ENTITYSTORAGE_H
