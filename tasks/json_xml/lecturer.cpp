#include "lecturer.h"

Group::Group(QString name){
    this->groupName = name;
}

void Group::setGroupName(QString name){
    this->groupName = name;
}

QString Group::getGroupName(){
    return this->groupName;
}

void Group::printGroup() {
      std::cout << "     " << "{" << this->groupName.toStdString() << "}" << endl;
}

Lecturer::Lecturer(QString name, int expr, double rating, vector<Group> &groups) {
    this->name = name;
    this->expr = expr;
    this->rating = rating;
    this->groups = groups;
}

void Lecturer::setName(QString name) {
    this->name = name;
}

QString Lecturer::getName() {
    return this->name;
}

void Lecturer::setExpr(int expr) {
    this->expr = expr;
}

int Lecturer::getExpr() {
    return this->expr;
}

void Lecturer::setRating(double rating) {
    this->rating = rating;
}

double Lecturer::getRating() {
    return this->rating;
}

void Lecturer::addGroup(Group group) {
    this->groups.push_back(group);
}

void Lecturer::setGroups(vector<Group> & groups) {
    this->groups = groups;
}

void Lecturer::printLecturer() {
    std::cout << "Name: "  << this->name.toStdString() << endl
              << "Experience: "  << this->expr << endl
              << "Rating: " << this->rating << endl;
    std::cout << "Groups: " << endl;
    for(Group & l: this->groups){
        l.printGroup();
    }
    std::cout << "-----------------------" << endl;
}

vector<Group> & Lecturer::getGroups() {
    return this->groups;
}
