#include <tree.h>
#include <string_buffer.h>
#include <string.h>


Tree * Tree_new(void * value) {
    Tree * self = malloc(sizeof(Tree));
    self->value = value;
    self->children = List_new();
    return self;
}

void Tree_free(Tree * self) {
    //AstNode_free(self->value);
    List_free(self->children);
    free(self);
}

void Tree_clearChildren(Tree *curNode) {
    List * children = curNode->children;
    size_t count = List_count(children);
    for (int i = 0; i < count; i++) {
        void * child = List_at(children, i);
        Tree_clearChildren(child);
    }
    Tree_free(curNode);
}

static char * str_append(const char * str, const char * append) {
    size_t newLen = strlen(str) + strlen(append) + 1;
    char * buf = malloc(sizeof(char) * newLen);
    buf[0] = '\0';
    sprintf(buf, "%s%s", str, append);
    return buf;
}

static void print(FILE *f, Tree *node, const char *indent, int root, int last)
{
    fwrite(indent, strlen(indent), 1, f);
    char *newIndent = NULL;
    if (last)
    {
        if (!root)
        {
            fwrite("└>", strlen("└>"), 1, f);
            newIndent = str_append(indent, "❤ ❤ ");
        }
        else
        {
            newIndent = str_append(indent, "");
        }
    }
    else
    {
        fwrite("|", strlen("|"), 1, f);
        newIndent = str_append(indent, "|");
    }
    AstNode * astNode = node->value;
    fwrite(astNode->name, strlen(astNode->name), 1, f);
    fwrite("\n", strlen("\n"), 1, f);
    List * children = node->children;
    size_t count = List_count(children);
    for (int i = 0; i < count; i++) {
        void * child = List_at(children, i);
        print(f, child, newIndent, 0, i == count - 1);
    }
    free(newIndent);
}

void Tree_printToFile(FILE *f, Tree * astTree) {
    char * indent = strdup("");
    print(f, astTree, indent, 1, 1);
    free(indent);
}