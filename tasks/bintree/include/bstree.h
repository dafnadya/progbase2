#pragma once

typedef struct __BSTree BSTree;

BSTree * BSTree_new(void);
void BSTree_free(BSTree * self);

void BSTree_insert(BSTree * self, int key);
void BSTree_clear(BSTree * self);

void BSTree_printFormat(BSTree * self);
void BSTree_printTraverse(BSTree * self); 