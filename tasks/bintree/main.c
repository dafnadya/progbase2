#include <bintree.h>
#include <bstree.h>
 
 
int main(void) {
    int vals[] = {6, 3, 5, 1, 10, 13};
    BSTree * bst = BSTree_new();
    for (int i = 0; i < sizeof(vals)/ sizeof(vals[0]); i++) {
        BSTree_insert(bst, vals[i]);
    }
    BSTree_printFormat(bst);
    BSTree_printTraverse(bst);
    puts("");
    BSTree_clear(bst);
    BSTree_free(bst);
    return 0;
}