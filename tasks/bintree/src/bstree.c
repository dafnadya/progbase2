#include <bintree.h>
#include <bstree.h>
#include <assert.h> 
 
struct __BSTree
{
    BinTree *root;
};

BSTree *BSTree_new(void)
{
    BSTree *self = malloc(sizeof(BSTree));
    self->root = NULL;
    return self;
}
void BSTree_free(BSTree *self)
{
    free(self);
}

static void insert(BinTree *node, int key)
{
    assert(node != NULL);
    if (key < node->value)
    {
        if (node->left)
        {
            insert(node->left, key);
        }
        else
        {
            node->left = BinTree_new(key);
        }
    }
    else if (key > node->value)
    {
        if (node->right)
        {
            insert(node->right, key);
        }
        else
        {
            node->right = BinTree_new(key);
        }
    }
    else
        assert(0 && "Duplicate keys in BST");
}

void BSTree_insert(BSTree *self, int key)
{
    if (self->root)
        insert(self->root, key);
    else
        self->root = BinTree_new(key);
}

static void clear(BinTree * node) {
    if (node->left) {
        clear(node->left);
    }
    if (node->right) {
        clear(node->right);
    }
    BinTree_free(node);
}

void BSTree_clear(BSTree *self)
{
    clear(self->root);
}

//extra

static void print(BinTree * node, int level) {
    for(int i = 0; i < level; i++) {
        putchar('.');
        putchar('.');
    }
    if(node == NULL) {
        printf("(null)\n");
    }
    else {
        printf("%i\n", node->value);
        if(node->left || node->right) {
            print(node->left, level + 1);
            print(node->right, level + 1);
        }
    }
}

void BSTree_printFormat(BSTree *self)
{
    print(self->root, 0);
}

static void inOrder(BinTree * node) {
    assert(node != NULL);
    if (node->left) {
        inOrder(node->left);
    }
    printf("%i, ", node->value);
    if (node->right) {
        inOrder(node->right);
    }
}

void BSTree_printTraverse(BSTree *self)
{
    if (self->root) {
        inOrder(self->root);
    }
    else {
        printf("empty");
    }
}