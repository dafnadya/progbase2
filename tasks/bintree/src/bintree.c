#include <bintree.h>

BinTree * BinTree_new(int value) {
    BinTree * self = malloc(sizeof(BinTree));
    if (self == NULL) {
        abort();
    }
    self->value = value;
    self->left = NULL;
    self->right = NULL;
    return self;
}
void BinTree_free(BinTree * self) {
    free(self);
}