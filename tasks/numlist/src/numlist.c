#include <numlist.h>
#include <stdio.h>
#include <stdlib.h>

struct __NumList
{
    int *array;
    int length;
    int capacity;
};

static const int CAPACITY = 10;

NumList *NumList_new(void)
{
    NumList *self = malloc(sizeof(NumList));

    if (self == NULL)
    {
        fwrite("Out of memory", 1, 14, stderr);
        abort();
    }

    self->array = malloc(sizeof(int) * CAPACITY);

    if (self->array == NULL)
    {
        fwrite("Out of memory", 1, 14, stderr);
        abort();
    }

    self->capacity = CAPACITY;
    self->length = 0;
    return self;
}

void NumList_free(NumList *self)
{
    if (self == NULL)
    {
        assert(0 && "Argument is NULL");
    }
    free(self->array);
    free(self);
}

void NumList_add(NumList *self, int value)
{
    if (self == NULL)
    {
        assert(0 && "Argument is NULL");
    }
    if (self->length == self->capacity)
    {
        assert(0 && "Array is full");
    }
    self->array[self->length] = value;
    self->length++;
}

int NumList_removeAt(NumList *self, size_t index)
{
    if (index < 0 || index >= self->length)
    {
        assert(0 && "Invalid index");
        fprintf(stderr, "Invalid index");
    }
    for (int i = index + 1; i < self->length; i++)
    {
        self->array[i - 1] = self->array[i];
    }
    self->length--;
    return self->length;
}

size_t NumList_count(NumList *self)
{
    if (self == NULL)
    {
        assert(0 && "Argument is NULL");
    }
    return self->length;
}

int NumList_at(NumList *self, size_t index)
{
    if (self == NULL)
    {
        assert(0 && "Argument is NULL");
    }

    if (index < 0 || index >= self->length)
    {
        assert(0 && "Invalid index");
    }
    return self->array[index];
}

void NumList_insert(NumList *self, size_t index, int value)
{
    if (self == NULL)
    {
        assert(0 && "Argument is NULL");
    }
    if (index < 0 || index >= self->capacity || index > self->length)
    {
        assert(0 && "Invalid index");
    }
    if (index == self->length)
        NumList_add(self, value);
    else
    {
        int last = self->array[self->length - 1];
        for (int i = index + 1; i < self->length; i++)
            self->array[i] = self->array[i - 1];
        NumList_add(self, last);
        self->array[index] = value;
    }
}

/**
 *  @returns old value at index
 */
int NumList_set(NumList *self, size_t index, int value)
{
    if (self == NULL)
    {
        assert(0 && "Argument is NULL");
    }
    if (index < 0 || index >= self->length)
    {
        assert(0 && "Invalid index");
    }
    int oldValue = self->array[index];
    self->array[index] = value;
    return oldValue;
}

char * string = getFromFile(fileName);
            char buffer[100];
            NumList * list = NumList_new();
            for (int i = 0, k = 0; i < strlen(string); i++) {
                if (isdigit(string[i])) {
                while (isdigit(string[i])) {
                    buffer[k] = string[i];
                    i++;
                    k++;
                }
                int number = atoi(buffer);
                NumList_add(list, number);
                buffer[0] = '\0';
                k = 0;
                }
            }