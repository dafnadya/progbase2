#include <numlist_ext.h>

void NumList_print(NumList * self){
    if (self == NULL)
    {
        assert(0 && "Argument is NULL");
    }
    int count = NumList_count(self);
    for(int i = 0; i < count; i++)
        printf("%i ", NumList_at(self, i));
    puts("");
}

void NumList_removeNeg(NumList * self) {
    if (self == NULL)
    {
        assert(0 && "Argument is NULL");
    }
    int i = 0;
    while (i < NumList_count(self))
    {
        if (NumList_at(self, i) < 0)
            NumList_removeAt(self, i);
        else
            i++;
    }
}

//remove negative