#pragma once

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

typedef struct __NumList NumList;

NumList * NumList_new(void);
void NumList_free(NumList * self);

void NumList_add(NumList * self, int value);
int NumList_removeAt(NumList * self, size_t index);
size_t NumList_count(NumList * self);
int NumList_at(NumList * self, size_t index);
void NumList_insert(NumList * self, size_t index, int value);

/**
 *  @returns old value at index
 */
int NumList_set(NumList * self, size_t index, int value);