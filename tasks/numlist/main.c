#include <numlist.h>
#include <numlist_ext.h>
#include <time.h>

static const int CAPACITY = 10;

int main(void)
{

    NumList *self = NumList_new();

    srand(time(0));
    for (int i = 0; i < CAPACITY; i++)
    {
        NumList_add(self, rand() % 201 - 100);
    }
    NumList_print(self);

    NumList_removeNeg(self);
    NumList_print(self);

    NumList_free(self);
    return 0;
}