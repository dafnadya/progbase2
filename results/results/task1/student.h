#ifndef STUDENT_H
#define STUDENT_H

#include <QDateTime>

class Student
{
    int id;
    QString fullname;
    QDateTime birthday;
    double score;

public:
    Student();
    Student(int id, QString fullname, QDateTime birthday, double score);

    int getId();
    QString getName();
    QDateTime getDate();
    double getScore();

    void setId(int id);
    void setName(QString fullname);
    void setDate(QDateTime birthday);
    void setScore(double score);
};

#endif // STUDENT_H
