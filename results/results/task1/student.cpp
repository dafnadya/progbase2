#include "student.h"

Student::Student()
{

}

Student::Student(int id, QString fullname, QDateTime birthday, double score) {
    this->id = id;
    this->fullname = fullname;
    this->birthday = birthday;
    this->score = score;
}

int Student::getId() {
    return id;
}

QString Student::getName() {
    return fullname;
}

QDateTime Student::getDate() {
    return birthday;
}

double Student::getScore() {
    return score;
}

void Student::setId(int id) {
    this->id = id;
}

void Student::setName(QString fullname) {
    this->fullname = fullname;
}

void Student::setDate(QDateTime birthday) {
    this->birthday = birthday;
}

void Student::setScore(double score) {
    this->score = score;
}
