#include "list_oper.h"
#include <QDebug>
#include <iostream>

QList<int> sortNames(QList<Student> & list) {
    QList<Student> students = list;
    QList<int> ids;
    QList<QString> names;
    for (Student & s : students) {
        names.push_back(s.getName());
    }
    qSort(names.begin(), names.end());
    for (QString & str : names) {
        for (Student & s : students) {
            if (QString::compare(str, s.getName()) == 0) {
                ids.push_back(s.getId());
            }
        }
    }
    return ids;
}

double getAv(QList<Student> & list) {
    QList<Student> s = list;
    double sum = 0;
    for (Student & st : s) {
        sum += st.getScore();
    }

    if (s.count() > 0) {
        sum = sum / s.count();
    }
    return sum;
}
