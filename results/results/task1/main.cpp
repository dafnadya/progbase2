#include <QCoreApplication>
#include <iostream>
#include "entitystorage.h"
#include "list_oper.h"

using namespace std;

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);
    QStringList args = a.arguments();
    if (args.count() != 2)
    {

        cout << "argument required" << endl;
          return 1;
    }
    QString fileName = args[1];
    cout << fileName.toStdString() << endl;

    QList<Student> list;
    QDate date = QDate(2000, 1, 23);
    QDate date2 = QDate(2002, 2, 11);
    QTime time = QTime(11, 33);
    QDateTime bday = QDateTime(date, time);
    QDateTime bday2 = QDateTime(date2, time);
    list.push_back(Student(1, "masha", bday, 13.43));
    list.push_back(Student(2, "madd", bday2, 43.55));

    //load(fileName.toStdString());
    saveResults(list, fileName.toStdString());
    return 0;
}
