#ifndef LIST_OPER_H
#define LIST_OPER_H

#include "entitystorage.h"
#include <QtAlgorithms>

QList<int> sortNames(QList<Student> & list);
double getAv(QList<Student> & list);

#endif // LIST_OPER_H
