#ifndef ENTITYSTORAGE_H
#define ENTITYSTORAGE_H

#include <string>
#include <vector>
#include "student.h"
#include <QtXml>

using namespace std;

QList<Student> load(string fileName);
void save(QList<Student> & list, string fileName);

#endif // ENTITYSTORAGE_H
