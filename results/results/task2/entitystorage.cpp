#include "entitystorage.h"

#include <fstream>

using namespace std;

string studentsToJsonString(QList<Student> & list) {
    QJsonDocument doc;
    QList<Student> vec = list;
    QJsonArray dinosaursArr;

    for(Student  & l: vec){
        QJsonObject dinosaurObj;
        dinosaurObj.insert("id", l.getId());
        dinosaurObj.insert("fullname", l.getName());
        dinosaurObj.insert("birthday", l.getDate().toString(Qt::ISODate));
        dinosaurObj.insert("score", l.getScore());
        dinosaursArr.push_back(dinosaurObj);
    }
    doc.setArray(dinosaursArr);
    return doc.toJson().toStdString();
}

QList<Student> jsonStringToStudents(string & str) {
    QJsonParseError err;
    QJsonDocument doc = QJsonDocument::fromJson(
           QByteArray::fromStdString(str),
           &err);
    if (err.error != QJsonParseError::NoError) {
        return QList<Student>();
    }
    QList<Student> vec;
    QJsonArray DinosaursArr = doc.array();
    for (int i = 0; i < DinosaursArr.size(); i++) {
        Student lect;
        QJsonValue value = DinosaursArr.at(i);
        QJsonObject lectObj = value.toObject();
        lect.setId(lectObj.value("id").toInt());
        lect.setName(lectObj.value("fullname").toString());
        lect.setDate(QDateTime::fromString(lectObj.value("birthday").toString(), Qt::ISODate));
        lect.setScore(lectObj.value("score").toDouble());
        vec.push_back(lect);
     }
    return vec;
}

void save(QList<Student> & list, string fileName) {
    QList<Student> stud = list;
    ofstream file(fileName);
    string str = studentsToJsonString(stud);
    file << str << endl;
    file.close();
}

QList<Student> load(string fileName){
    QList<Student> r = QList<Student>();
    ifstream file(fileName);
    if(!file.is_open()) {
        return r;
    }

    string str((istreambuf_iterator<char>(file)), istreambuf_iterator<char>());
    r = jsonStringToStudents(str);
    file.close();
    return r;
}
