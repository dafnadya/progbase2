#ifndef STUDENT_H
#define STUDENT_H

#include <QDateTime>
#include <QMetaType>

class Student
{
    int id;
    QString fullname;
    QDateTime birthday;
    double score;

public:
    Student();
    Student(int id, QString fullname, QDateTime birthday, double score);

    int getId();
    QString getName();
    QDateTime getDate();
    double getScore();

    void setId(int id);
    void setName(QString fullname);
    void setDate(QDateTime birthday);
    void setScore(double score);
};

Q_DECLARE_METATYPE(Student)

#endif // STUDENT_H
