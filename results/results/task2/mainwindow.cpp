#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QFileDialog>
#include "entitystorage.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    ui->list->clear();
    delete ui;
}

void MainWindow::on_saveButton_clicked()
{
    QString path = QFileDialog::getSaveFileName(
                this,
                tr("Save File"),
                "../task2",
                tr("JSON files (*.json)"));
    if (!path.isEmpty()) {
        QList<Student> stList = QList<Student>();
        for (int i = 0; i < ui->list->count(); i++) {
            QListWidgetItem * item = ui->list->item(i);
            QVariant var = item->data(Qt::UserRole);
            Student st = var.value<Student>();
            stList.push_back(st);
        }
        save(stList, path.toStdString());
    }
}

void MainWindow::on_loadButton_clicked()
{
    QString path = QFileDialog::getOpenFileName(
                this,
                tr("Open File"),
                "../task2",
                tr("JSON files (*.json)"));
    if (!path.isEmpty()) {
        ui->list->clear();
        QList<Student> stList = load(path.toStdString());
        for (Student &l : stList) {
            QListWidgetItem *item = new QListWidgetItem;
            item->setText(l.getName());
            QVariant var;
            var.setValue(l);
            item->setData(Qt::UserRole, var);
            ui->list->addItem(item);
        }
    }
}

void MainWindow::on_list_itemSelectionChanged()
{
    QList<QListWidgetItem *> selectedItem = ui->list->selectedItems();
    if (selectedItem.count() == 1) {
            QListWidgetItem * selected = selectedItem.at(0);
            QVariant var = selected->data(Qt::UserRole);
            Student d = var.value<Student>();
            ui->name->setText(d.getName());
            ui->id->setText(QString::number(d.getId()));
            ui->bday->setText(d.getDate().toString());
            ui->score->setText(QString::number(d.getScore()));
        }
    else {
        ui->name->setText("");
        ui->id->setText("");
        ui->bday->setText("");
        ui->score->setText("");
     }
}
